/////////////////////////////////////////////////////////////////////
// TestExecutive.cpp -	Text Executive to test the Remote Publisher//
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:   Code Analyzer  CSE687 Pr3, Apr-4	               //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu				   //
/////////////////////////////////////////////////////////////////////

#include "../Publisher/Publisher.h"
#include "../DependencyAnalysis/DependancyAnalysis.h"
#include "../TypeTable/TypeTable.h"
#include "../Analyzer/Executive.h"
#include <iostream>
#include "CodePublisher.h"

void CodePublisher::generateHTM(char * directory)
{
	char* argv[] = { directory,"*.h","*.cpp" };
	using namespace CodeAnalysis;
	CodeAnalysisExecutive exec;
	bool succeeded = exec.ProcessCommandLine(0, argv);
	if (!succeeded)
	{
		return;
	}
	exec.getSourceFiles();
	exec.processSourceCode(true);
	TypeAnal1 ta;
	ta.doTypeAnal();
	DependencyAnalysis analyseFiles;
	Publisher publisher;
	ASTNode* pAstRoot = analyseFiles.getFilesFromDir();
	std::unordered_map<std::string, std::set<std::string>> htmlDepTable = analyseFiles.DependencyAnalyzer(ta.accessMyTypeTable(), pAstRoot);
	publisher.ASTParser(pAstRoot);
	analyseFiles.displayDepencyTable();
	bool flag = publisher.fileConverter("../Test", htmlDepTable, ta.accessMyTypeTable());
	if (!flag) { std::cout << "Error in opening File"; }
	//publisher.GenerateIndexList();
	//std::string fileArg = argv[argc - 1];
	//std::string indexfile = "../../HTMFiles/"+ fileArg;
	//std::string command("start \"\" \"" + indexfile + "\"");
	//std::system(command.c_str());
}

#ifdef publisherCode
int main() {
	CodePublisher cp;
	std::string category = "../Test";
	char *cat = new char[category.length() + 1];
	cp.generateHTMFiles(cat);
}
#endif // DEBUG


