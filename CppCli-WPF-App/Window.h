#ifndef WINDOW_H
#define WINDOW_H
///////////////////////////////////////////////////////////////////////////
// Window.h - GUI using Managed C++ (Defines UI Element Properties)      //
//          -	 687 Project #4										     //
// ver 2.0                                                               //
// Omkar Patil, CSE687 - Object Oriented Design, Spring 2015             //
///////////////////////////////////////////////////////////////////////////
/*
*  Package Operations:
*  -------------------
*  This package Defines the UI elements and it processing on clieck of events
	and sends the messages to mock channel to process and receives messages from mock channel
	and displays them on GUI
*
  Public Interfaces:
  void setUpStatusBar();		//sets up status bar
  void setUpTabControl();		//sets up control events for tabs
  void setUpSendMessageView();	//sets the send files tab UI Elements
  void setUpFileListView();		//sets the Publish Files tab of UI element
  void setUpConnectionView();	//sets up the connection view
  void sendMessage(Object^ obj, RoutedEventArgs^ args);		//sends the files request
  void addText(String^ msg);		//display the msgs on GUI 
  void getMessage();				//receives msgs from the mock channel receiver
  void clear(Object^ sender, RoutedEventArgs^ args);	//clears the files displayed in the list box
  void getItemsFromList(Object^ sender, RoutedEventArgs^ args);	//gets the category from list
  void publishFiles(Object^ sender, RoutedEventArgs^ args);	//sends request for publish files
  void GetAllFilesCateogory(Object^ sender, RoutedEventArgs^ args);	//gets all files using category
  void NoParentListCategory(Object^ sender, RoutedEventArgs^ args); /gets no parent list using category
  void selectDirectory(Object^ sender, RoutedEventArgs^ args);	//selects directory

*  Required Files:
*  ---------------
*  Window.h, Window.cpp, MochChannel.h, MochChannel.cpp,
*  Cpp11-BlockingQueue.h, Cpp11-BlockingQueue.cpp
*
*  Build Command:
*  --------------
*  devenv CppCli-WPF-App.sln
*  - this builds C++\CLI client application and native mock channel DLL
*
*  Maintenance History:
*  --------------------
	ver 3.1 04 May 2017
	-Added UI Elements like Send Files, delete, Get all files, Clear, Select directory
		publish and No parent list buttons and a combo box for category selection
*  ver 3.0 : 22 Apr 2016
*  - added support for multi selection of Listbox items.  Implemented by
*    Saurabh Patel.  Thanks Saurabh.
*  ver 2.0 : 15 Apr 2015
*  - completed message passing demo with moch channel
*  - added FileBrowserDialog to show files in a selected directory
*  ver 1.0 : 13 Apr 2015
*  - incomplete demo with GUI but not connected to mock channel
*/
/*
*/
using namespace System;
using namespace System::Text;
using namespace System::Windows;
using namespace System::Windows::Input;
using namespace System::Windows::Markup;
using namespace System::Windows::Media;                   // TextBlock formatting
using namespace System::Windows::Controls;                // TabControl
using namespace System::Windows::Controls::Primitives;    // StatusBar
using namespace System::Threading;
using namespace System::Threading::Tasks;
using namespace System::Windows::Threading;
using namespace System::ComponentModel;

#include "../MockChannel/MockChannel.h"
#include <iostream>

namespace CppCliWindows
{
  ref class WPFCppCliDemo : Window
  {
    // MockChannel references

    ISendr* pSendr_;
    IRecvr* pRecvr_;
    IMockChannel* pChann_;

    // Controls for Window
    DockPanel^ hDockPanel = gcnew DockPanel();      // support docking statusbar at bottom
    Grid^ hGrid = gcnew Grid();                    
    TabControl^ hTabControl = gcnew TabControl();
    TabItem^ hSendMessageTab = gcnew TabItem();
    TabItem^ hFileListTab = gcnew TabItem();
    TabItem^ hLogsTab = gcnew TabItem();
	StatusBar^ hStatusBar = gcnew StatusBar();
    StatusBarItem^ hStatusBarItem = gcnew StatusBarItem();
    TextBlock^ hStatus = gcnew TextBlock();

    // Controls for SendMessage View	(First Tab)
	Grid^ hSendMessageGrid = gcnew Grid();
    Button^ hSendButton = gcnew Button();
    Button^ hClearButton = gcnew Button();
    TextBlock^ hTextBlock1 = gcnew TextBlock();
    ScrollViewer^ hScrollViewer1 = gcnew ScrollViewer();
    StackPanel^ hStackPanel1 = gcnew StackPanel();
	StackPanel^ hStackPanel2 = gcnew StackPanel();
	Forms::FolderBrowserDialog^ browseFilesDialog = gcnew Forms::FolderBrowserDialog();
	Button^ selectFilesButton = gcnew Button();
	ListBox^ filesListBox = gcnew ListBox();
	ComboBox^ categoryList = gcnew ComboBox();
	Button^ GetAllFilesButton = gcnew Button();
	Button^ NoParentListButton = gcnew Button();

    // Controls for Publish View
    Grid^ hFileListGrid = gcnew Grid();
    Forms::FolderBrowserDialog^ hFolderBrowserDialog = gcnew Forms::FolderBrowserDialog();
    ListBox^ hListBox = gcnew ListBox();
    Button^ pusblishButton = gcnew Button();
	Button^ DeleteButton = gcnew Button();
	String^ msgText = "";
    Thread^ recvThread;

  public:
    WPFCppCliDemo();
    ~WPFCppCliDemo();
    void setUpStatusBar();
    void setUpTabControl();
    void setUpSendMessageView();
    void setUpFileListView();
    void setUpConnectionView();
    void sendMessage(Object^ obj, RoutedEventArgs^ args);
    void addText(String^ msg);
    void getMessage();
    void clear(Object^ sender, RoutedEventArgs^ args);
	void getItemsFromList(Object^ sender, RoutedEventArgs^ args);
    void publishFiles(Object^ sender, RoutedEventArgs^ args);
	void deleteFiles(Object^ sender, RoutedEventArgs^ args);
    void OnLoaded(Object^ sender, RoutedEventArgs^ args);
    void Unloading(Object^ sender, System::ComponentModel::CancelEventArgs^ args);
	void GetAllFilesCateogory(Object^ sender, RoutedEventArgs^ args);
	void NoParentListCategory(Object^ sender, RoutedEventArgs^ args);
	void selectDirectory(Object^ sender, RoutedEventArgs^ args);
  private:
    std::string toStdString(String^ pStr);
    String^ toSystemString(std::string& str);
    void setTextBlockProperties();
    void setButtonsProperties();
	void WPFCppCliDemo::GetAllNoParentButtonProperties();
  };
}


#endif
