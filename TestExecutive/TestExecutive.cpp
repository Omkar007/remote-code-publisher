/////////////////////////////////////////////////////////////////////
//  TestExecutive.cpp -  Testing Remote Code Publisher			   //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:    Code Analyzer  CSE687 Pr4, May-4               //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu				   //
/////////////////////////////////////////////////////////////////////

#include "../HttpMessage/HttpMessage.h"
#include "../Sockets/Sockets.h"
#include "../FileSystem/FileSystem.h"
#include "../Logger/Logger.h"
#include "../Utilities/Utilities.h"
#include "../MsgClient/ClientSendRecv.h"
#include <string>
#include <iostream>
#include <sstream>
#include <thread>

//Test Stub
#ifdef DEBUG
int main()
{
	BlockingQueue<HttpMessage> recvQ;

	std::string files = "..\\..\\Test\\Child.h ..\\..\\Test\\Child2.h ..\\..\\Test\\Grandparent.h ..\\..\\Test\\Parent.cpp ..\\..\\Test\\Parent.h ..\\..\\Test\\Test.cpp";
	std::string cat = "Category-A";
	std::string file = "..\\..\\Test\\Child.h";
	std::string delFile = "..\\..\\Test\\Child2.h";
	MsgClient c1;
	std::thread t2(
		[&]() {
		c1.downloadCSSJSFiles("CssJsFiles");
		c1.execute(cat, files);
		c1.sendPublishMsg(cat, file);
		c1.sendDeleteRequest(cat, delFile);
		c1.sendGetAllCategoriesFiles(cat);
		c1.sendNoparentRequestList(cat);
	}
	);
	t2.join();

	SocketSystem ss;
	SocketListener sl(8081, Socket::IP6);
	ServerHandler cp(recvQ);
	sl.start(cp);
	std::thread t1;
	while (true)
	{
		HttpMessage msg = recvQ.deQ();
		std::thread t1(
			[&]() {
			cp.processHandler(msg);
		}
		);
		t1.join();
	}
}
#endif // DEBUG



