/////////////////////////////////////////////////////////////////////
// Window.cpp -  Defines the GUI Elements						   //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:    Code Analyzer  CSE687 Pr4, May-4               //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu				   //
/////////////////////////////////////////////////////////////////////
/*
*/
#include "Window.h"
using namespace CppCliWindows;

//function that defines the event hanlder for buttons and intializes queues 
WPFCppCliDemo::WPFCppCliDemo()
{ ObjectFactory* pObjFact = new ObjectFactory;
  pSendr_ = pObjFact->createSendr();
  pRecvr_ = pObjFact->createRecvr();
  pChann_ = pObjFact->createMockChannel(pSendr_, pRecvr_);
  pChann_->start();
  delete pObjFact;
  recvThread = gcnew Thread(gcnew ThreadStart(this, &WPFCppCliDemo::getMessage));
  recvThread->Start();
  this->Loaded += gcnew System::Windows::RoutedEventHandler(this, &WPFCppCliDemo::OnLoaded);
  this->Closing += gcnew CancelEventHandler(this, &WPFCppCliDemo::Unloading);
  hSendButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::sendMessage);
  hClearButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::clear);
  pusblishButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::publishFiles);
  selectFilesButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::selectDirectory);
  GetAllFilesButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::GetAllFilesCateogory);
  NoParentListButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::NoParentListCategory);
  DeleteButton->Click += gcnew RoutedEventHandler(this, &WPFCppCliDemo::deleteFiles);
  this->Title = "Remote Code Publisher";
  this->Width = 800;
  this->Height = 600;
  this->Content = hDockPanel;
  hDockPanel->Children->Add(hStatusBar);
  hDockPanel->SetDock(hStatusBar, Dock::Bottom);
  hDockPanel->Children->Add(hGrid);
  setUpTabControl();
  setUpStatusBar();
  setUpSendMessageView();
  setUpFileListView();
  setUpConnectionView();
}

//destructor
WPFCppCliDemo::~WPFCppCliDemo()
{
  delete pChann_;
  delete pSendr_;
  delete pRecvr_;
}

//function used to set up the status bar
void WPFCppCliDemo::setUpStatusBar()
{
  hStatusBar->Items->Add(hStatusBarItem);
  hStatus->Text = "Note:List Box is used to display the files to be process click on file to publish or delete";
  hStatusBarItem->Content = hStatus;
  hStatusBar->Padding = Thickness(10, 2, 10, 2);
}

//Function used to setup tabs
void WPFCppCliDemo::setUpTabControl()
{
  hGrid->Children->Add(hTabControl);
  hSendMessageTab->Header = "Send Files";
  hFileListTab->Header = "Publish Files";
  hTabControl->Items->Add(hSendMessageTab);
  hTabControl->Items->Add(hFileListTab);
}

//setting textbox properties for the Send Files Tab View
void WPFCppCliDemo::setTextBlockProperties()
{
  RowDefinition^ hRow1Def = gcnew RowDefinition();
  hSendMessageGrid->RowDefinitions->Add(hRow1Def);
  Border^ hBorder1 = gcnew Border();
  hBorder1->BorderThickness = Thickness(1);
  hBorder1->BorderBrush = Brushes::Black;
  hBorder1->Child = filesListBox;
  filesListBox->Padding = Thickness(15);
  filesListBox->SelectionMode = SelectionMode::Multiple;
  filesListBox->FontFamily = gcnew Windows::Media::FontFamily("Tahoma");
  filesListBox->FontWeight = FontWeights::Bold;
  filesListBox->FontSize = 12;
  hScrollViewer1->VerticalScrollBarVisibility = ScrollBarVisibility::Auto;
  hScrollViewer1->Content = hBorder1; 
  hSendMessageGrid->SetRow(hScrollViewer1, 0);
  hSendMessageGrid->Children->Add(hScrollViewer1);
}

//Setting Send Files and Clear Files Button Properties in Send View Message Tab
void WPFCppCliDemo::setButtonsProperties()
{
  RowDefinition^ hRow2Def = gcnew RowDefinition();
  hRow2Def->Height = GridLength(15);
  hSendMessageGrid->RowDefinitions->Add(hRow2Def);
  hSendButton->Content = "Send Files";
  Border^ hBorder2 = gcnew Border();
  hBorder2->Width = 120;
  hBorder2->Height = 30;
  hBorder2->BorderThickness = Thickness(2);
  hBorder2->BorderBrush = Brushes::Black;
  hClearButton->Content = "Clear";
  hBorder2->Child = hSendButton;
  Border^ hBorder3 = gcnew Border();
  hBorder3->Width = 120;
  hBorder3->Height = 30;
  hBorder3->BorderThickness = Thickness(2);
  hBorder3->BorderBrush = Brushes::Black;
  hBorder3->Child = hClearButton;
  hStackPanel1->Children->Add(hBorder2);
  TextBlock^ hSpacer = gcnew TextBlock();
  hSpacer->Width = 10;
  hStackPanel1->Children->Add(hSpacer);
  hStackPanel1->Children->Add(hBorder3);
  hStackPanel1->Orientation = Orientation::Horizontal;
  hStackPanel1->HorizontalAlignment = System::Windows::HorizontalAlignment::Center;
  hSendMessageGrid->SetRow(hStackPanel1, 4);
  hSendMessageGrid->Children->Add(hStackPanel1);
}

//Setting Send Files and Clear Files Button Properties in Send View Message Tab
void WPFCppCliDemo::GetAllNoParentButtonProperties()
{
	RowDefinition^ hrow5 = gcnew RowDefinition();
	hrow5->Height = GridLength(45);
	hSendMessageGrid->RowDefinitions->Add(hrow5);
	GetAllFilesButton->Content = "Get Files by Category";
	Border^ hBorder2 = gcnew Border();
	hBorder2->Width = 120;
	hBorder2->Height = 30;
	hBorder2->BorderThickness = Thickness(2);
	hBorder2->BorderBrush = Brushes::Black;
	NoParentListButton->Content = "Get No Parent List";
	hBorder2->Child = GetAllFilesButton;
	Border^ hBorder3 = gcnew Border();
	hBorder3->Width = 120;
	hBorder3->Height = 30;
	hBorder3->BorderThickness = Thickness(2);
	hBorder3->BorderBrush = Brushes::Black;
	hBorder3->Child = NoParentListButton;
	hStackPanel2->Children->Add(hBorder2);
	TextBlock^ hSpacer = gcnew TextBlock();
	hSpacer->Width = 10;
	hStackPanel2->Children->Add(hSpacer);
	hStackPanel2->Children->Add(hBorder3);
	hStackPanel2->Orientation = Orientation::Horizontal;
	hStackPanel2->HorizontalAlignment = System::Windows::HorizontalAlignment::Center;
	hSendMessageGrid->SetRow(hStackPanel2, 5);
	hSendMessageGrid->Children->Add(hStackPanel2);
}

//Setting up the Send Files View Tab
void WPFCppCliDemo::setUpSendMessageView()
{ Console::Write("\n Setting up Send Files View \n");
  hSendMessageGrid->Margin = Thickness(20);
  hSendMessageTab->Content = hSendMessageGrid;
  setTextBlockProperties();
  setButtonsProperties();
  GetAllNoParentButtonProperties();
  RowDefinition^ hRow2Def2 = gcnew RowDefinition();
  hRow2Def2->Height = GridLength(45);
  hSendMessageGrid->RowDefinitions->Add(hRow2Def2);
  RowDefinition^ hrow3 = gcnew RowDefinition();
  hrow3->Height = GridLength(45);
  hSendMessageGrid->RowDefinitions->Add(hrow3);
  RowDefinition^ hrow4 = gcnew RowDefinition();
  hrow4->Height = GridLength(45);
  hSendMessageGrid->RowDefinitions->Add(hrow4);
  this->categoryList->SelectedIndex = 0;
  categoryList->Items->Add("Category-A");
  categoryList->Items->Add("Category-B");
  categoryList->Items->Add("Category-C");
  hSendMessageGrid->SetRow(categoryList, 2);
  hSendMessageGrid->Children->Add(categoryList);
  browseFilesDialog->ShowNewFolderButton = false;
  browseFilesDialog->SelectedPath = System::IO::Directory::GetCurrentDirectory();
  selectFilesButton->Content = "Select Directory";
  selectFilesButton->Height = 30;
  selectFilesButton->Width = 120;
  selectFilesButton->BorderThickness = Thickness(2);
  selectFilesButton->BorderBrush = Brushes::Black;
  hSendMessageGrid->SetRow(selectFilesButton, 3);
  hSendMessageGrid->Children->Add(selectFilesButton);
}

//function used to conevert system string to std string
std::string WPFCppCliDemo::toStdString(String^ pStr)
{
  std::string dst;
  for (int i = 0; i < pStr->Length; ++i)
    dst += (char)pStr[i];
  return dst;
}

//function used to sned files to server using mock channel
void WPFCppCliDemo::sendMessage(Object^ obj, RoutedEventArgs^ args)
{
	String^ files = "";
	Object^ category = categoryList->SelectedItem;
	int count = filesListBox->SelectedItems->Count;
	if (count > 0) {
		for each (String^ item in filesListBox->SelectedItems)
		{	files=files+" "+item;}
	}
	pSendr_->postMessage(toStdString("sendFiles;" + category->ToString() + files));
}

//function to convert the std:string to system string
String^ WPFCppCliDemo::toSystemString(std::string& str)
{
  StringBuilder^ pStr = gcnew StringBuilder();
  for (size_t i = 0; i < str.size(); ++i)
    pStr->Append((Char)str[i]);
  return pStr->ToString();
}

//function to add the elements to list box
void WPFCppCliDemo::addText(String^ msg)
{
	array<String^>^ tokens = msg->Split(L' ');
	//hListBox->Items->Clear();
	for (int i = 1; i<tokens->Length; i++)
		hListBox->Items->Add(tokens[i]);
}

//function to dequeue Messages sent from Server and update UI
void WPFCppCliDemo::getMessage()
{
  while (true)
  {
    std::cout << "\n  receive thread calling getMessage()";
    std::string msg = pRecvr_->getMessage();
    String^ sMsg = toSystemString(msg);
    array<String^>^ args = gcnew array<String^>(1);
    args[0] = sMsg;

    Action<String^>^ act = gcnew Action<String^>(this, &WPFCppCliDemo::addText);
    Dispatcher->Invoke(act, args);
  }
}

//function used to Clear the window
void WPFCppCliDemo::clear(Object^ sender, RoutedEventArgs^ args)
{
  Console::Write("\n Testing all Functioanlities ");
  filesListBox->Items->Clear();
}

//function used to Get All Files from the server usign mock channel
void WPFCppCliDemo::GetAllFilesCateogory(Object^ sender, RoutedEventArgs^ args)
{
	Console::Write("\nGet All Files clicked \n");
	Object^ category = categoryList->SelectedItem;
	pSendr_->postMessage(toStdString("getAllFiles;" + category->ToString()));

}

//function used to Get NO Parent List from the server usign mock channel
void WPFCppCliDemo::NoParentListCategory(Object^ sender, RoutedEventArgs^ args)
{
	Console::Write("\n Get No Parent Files clicked \n");
	Object^ category = categoryList->SelectedItem;
	pSendr_->postMessage(toStdString("noParentList;" + category->ToString()));
}



//function to get the selected items from the list
void WPFCppCliDemo::getItemsFromList(Object^ sender, RoutedEventArgs^ args)
{
	int index = 0;
	int count = hListBox->SelectedItems->Count;
	hStatus->Text = "Show Selected Items";
	array<System::String^>^ items = gcnew array<String^>(count);
	if (count > 0) {
		for each (String^ item in hListBox->SelectedItems)
		{
			items[index++] = item;
		}
	}
	hListBox->Items->Clear();
	if (count > 0) {
		for each (String^ item in items)
		{
			hListBox->Items->Add(item);
		}
	}
}

//setting up the Publish File and Delete file Tab View
void WPFCppCliDemo::setUpFileListView()
{
  Console::Write("\n  setting up FileList view");
  hFileListGrid->Margin = Thickness(20);
  hFileListTab->Content = hFileListGrid;
  RowDefinition^ hRow1Def = gcnew RowDefinition();
  hFileListGrid->RowDefinitions->Add(hRow1Def);
  Border^ hBorder1 = gcnew Border();
  hBorder1->BorderThickness = Thickness(1);
  hBorder1->BorderBrush = Brushes::Black;
  hListBox->SelectionMode = SelectionMode::Single;
  hBorder1->Child = hListBox;
  hFileListGrid->SetRow(hBorder1, 0);
  hFileListGrid->Children->Add(hBorder1);
  RowDefinition^ hRow2Def = gcnew RowDefinition();
  hRow2Def->Height = GridLength(55);
  RowDefinition^ hRow2Def2 = gcnew RowDefinition();
  hRow2Def2->Height = GridLength(55);
  hFileListGrid->RowDefinitions->Add(hRow2Def);
  hFileListGrid->RowDefinitions->Add(hRow2Def2);
  pusblishButton->Content = "Publish File";
  pusblishButton->Height = 30;
  pusblishButton->Width = 120;
  pusblishButton->BorderThickness = Thickness(2);
  pusblishButton->BorderBrush = Brushes::Black;
  hFileListGrid->SetRow(pusblishButton, 1);
  hFileListGrid->Children->Add(pusblishButton);
  DeleteButton->Content = "Delete File";
  DeleteButton->Height = 30;
  DeleteButton->Width = 120;
  DeleteButton->BorderThickness = Thickness(2);
  DeleteButton->BorderBrush = Brushes::Black;
  hFileListGrid->SetRow(DeleteButton, 2);
  hFileListGrid->Children->Add(DeleteButton);
  hFolderBrowserDialog->ShowNewFolderButton = false;
  hFolderBrowserDialog->SelectedPath = System::IO::Directory::GetCurrentDirectory();
}

//function used to send the publish file request to the server
void WPFCppCliDemo::publishFiles(Object^ sender, RoutedEventArgs^ args)
{
	std::cout << "\n  Button Publish is clicked \n";
	Object^ category = categoryList->SelectedItem;
	Object^ item = hListBox->SelectedItem;
	pSendr_->postMessage(toStdString("publishFiles;" + category->ToString() + " " + item));
}

void CppCliWindows::WPFCppCliDemo::deleteFiles(Object ^ sender, RoutedEventArgs ^ args)
{
	std::cout << "\n  Button Delete is clicked \n";
	Object^ category = categoryList->SelectedItem;
	Object^ item = hListBox->SelectedItem;
	pSendr_->postMessage(toStdString("deleteFile;" + category->ToString() + " " + item));
}

//function used to select the directory
void WPFCppCliDemo::selectDirectory(Object^ sender, RoutedEventArgs^ args)
{
	std::cout << "\n  Browsing for folder \n";
	filesListBox->Items->Clear();
	System::Windows::Forms::DialogResult result;
	result = browseFilesDialog->ShowDialog();
	if (result == System::Windows::Forms::DialogResult::OK)
	{
		String^ path = browseFilesDialog->SelectedPath;
		array<String^>^ files = System::IO::Directory::GetFiles(path, L"*.*");
		for (int i = 0; i < files->Length; ++i)
			filesListBox->Items->Add(files[i]);
		array<String^>^ dirs = System::IO::Directory::GetDirectories(path);
		for (int i = 0; i < dirs->Length; ++i)
			filesListBox->Items->Add(L"<> " + dirs[i]);
	}
}

//function used to set up the connection view
void WPFCppCliDemo::setUpConnectionView()
{
  Console::Write("\n  setting up Connection view");
}

//function used to set up actions on window load
void WPFCppCliDemo::OnLoaded(Object^ sender, RoutedEventArgs^ args)
{
  Console::Write("\n  Window loaded");
}

//function used to set up actions on window close
void WPFCppCliDemo::Unloading(Object^ sender, System::ComponentModel::CancelEventArgs^ args)
{
  Console::Write("\n  Window closing");
}

[STAThread]
//int _stdcall WinMain()
int main(array<System::String^>^ args)
{
  Console::WriteLine(L"\n Starting Remote Code Publisher");
  Console::WriteLine(L"\n Req# 1,2 Please check code");
  Application^ app = gcnew Application();
  app->Run(gcnew WPFCppCliDemo());

  Console::WriteLine(L"\n\n");
}