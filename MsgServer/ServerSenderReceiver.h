#pragma once
/////////////////////////////////////////////////////////////////////////////////
// ServerSenderReceiver.h - Exposes Sender and Receiver Logic at Server end	   //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017				   //
//																			   //
//  Language:      Visual C++ 2015											   //
//  Platform:      HP Pavilion, Windows 10									   //
//  Application:   Dependency Analyzer CSE687 Pr4, Apr-4					   //
//  Author:        Omkar Patil, CST 4-187, Syracuse University				   //
//                 (315) 949-8810, ospatil@syr.edu							   //	
//////////////////////////////////////////////////////////////////////////////////
/*
* - Manual Information
* This package implements a server that receives HTTP style messages and
* files from multiple concurrent clients and simply displays the messages
* and stores files.

Public Interface:
=================
void downloadCSSJSS(HttpMessage& msg);					//downloads js css	
std::set<std::string> sendNoParentAckList(const std::string& category);	//send no parent list
void parentListAcknowledgement(const std::string & category);	//send acknoldegement msg for no parent list
void sendFilesReply(const std::string& msg);	//sends files reply msg
void sendAllFilesAcknowldement(const std::string& msg);	//send all files by category msg
void sendFileforPublishRequest(const std::string& category,const std::string& directory);	//process files for publishing and sends them
std::string GetfilesFromALLCategory(const std::string categories);	//get all files on basis of category
std::string GetExePath();	//gets the Exepath from where the code executes
std::string GetRepository(const std::string& category);	// defines the repositroy path
void sendPublishFileAcknowledgemnt(const std::string& msg);	//sends the publish files acknowldegemnt msg to client
std::string GetFile(const std::string& str);	//gets the file from the absolute path		
bool ProcessDeleteFileRequest(const std::string& category, const std::string& file); //deletes the file from server
void ProcessDeleteFileRequest(const std::string& file, bool flag);	//sends the delete msg acknowledgement to client
std::vector<std::string> AnalyzeFileDependencies(const std::string& file, std::unordered_map<std::string, std::set<std::string>> htmlDepTable);	//analyses the dependencies of file from dependency tables for lazy loading

void operator()(Socket socket);	//used for receiving files and msgs from socket
std::string GetExePath();	//gets exe path of execution
std::string GetRepository(const std::string& category); //gets the repository path	
HttpMessage readMessage(Socket& socket);	//reads the message from socket send by client
bool readFile(const std::string& category,const std::string& filename, size_t fileSize, Socket& socket); //reads file from client stream by stream 
std::string GetFile(const std::string& str); //gets the file from absolute path 
HttpMessage MessageParser(HttpMessage& msg, Socket& socket);	parses the file into html page

* - Build Process:
=================
Required files
MsgClient.cpp, MsgServer.cpp
*   HttpMessage.h, HttpMessage.cpp
*   Cpp11-BlockingQueue.h
*   Sockets.h, Sockets.cpp
*   FileSystem.h, FileSystem.cpp
*   Logger.h, Logger.cpp
*   Utilities.h, Utilities.cpp
Build commands
- devenv MsgServer.sln

* - Maintenance information
=================
ver 1.0 : 04 Apr 2017
-	first release
*/
#include <string>
#include <iostream>
#include <unordered_map>
#include "../HttpMessage/HttpMessage.h"
#include "../Sockets/Sockets.h"
#include "../RemotePublisher/RemotePublisher.h"

//This class is used as Receiver at Server End
class ClientHandler
{
public:
	ClientHandler(BlockingQueue<HttpMessage>& msgQ) : msgQ_(msgQ) {}
	void operator()(Socket socket);
	std::string GetExePath();
	std::string GetRepository(const std::string& category);
private:

	bool connectionClosed_;
	HttpMessage readMessage(Socket& socket);
	bool readFile(const std::string& category,const std::string& filename, size_t fileSize, Socket& socket);
	BlockingQueue<HttpMessage>& msgQ_;
	std::string GetFile(const std::string& str);
	HttpMessage MessageParser(HttpMessage& msg, Socket& socket);
};

//This class is used as Sender at Server End
class ServerSender {
public:
	void downloadCSSJSS(HttpMessage& msg);
	std::set<std::string> sendNoParentAckList(const std::string& category);
	void parentListAcknowledgement(const std::string & category);
	void sendFilesReply(const std::string& msg);
	void sendAllFilesAcknowldement(const std::string& msg);
	using EndPoint = std::string;
	void sendFileforPublishRequest(const std::string& category,const std::string& directory);
	std::string GetfilesFromALLCategory(const std::string categories);
	std::string GetExePath();
	std::string GetRepository(const std::string& category);
	void sendPublishFileAcknowledgemnt(const std::string& msg);
	std::string GetFile(const std::string& str);
	bool ProcessDeleteFileRequest(const std::string& category, const std::string& file);
	void ProcessDeleteFileRequest(const std::string& file, bool flag);
	std::vector<std::string> AnalyzeFileDependencies(const std::string& file, std::unordered_map<std::string, std::set<std::string>> htmlDepTable);
private:
	HttpMessage makeMessageforClient(size_t n, const std::string& msgBody, const EndPoint& ep);
	void sendMessageforClient(HttpMessage& msg, Socket& socket);
	bool sendFileforClient(const std::string& category,const std::string& fqname, Socket& socket);
	HttpMessage ServerSender::CreateDiffMsg(const std::string msgType, const EndPoint & myEndPoint, HttpMessage::Attribute attrib, const std::string & body, const EndPoint & ep);
	CodePublisher publish;
	std::unordered_map<std::string, std::set<std::string>> htmlDepTable;
};