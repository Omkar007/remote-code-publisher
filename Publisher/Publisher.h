#pragma once
/////////////////////////////////////////////////////////////////////////////////
// Publisher.h - Parses to .htm file and implements Show/Hide functionality    //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017				   //
//																			   //
//  Language:      Visual C++ 2015											   //
//  Platform:      HP Pavilion, Windows 10									   //
//  Application:   Dependency Analyzer CSE687 Pr3, Apr-4					   //
//  Author:        Omkar Patil, CST 4-187, Syracuse University				   //
//                 (315) 949-8810, ospatil@syr.edu							   //	
//////////////////////////////////////////////////////////////////////////////////
/*
* - Manual Information
This package analyzes files and converts them into .htm files so that this files can be published on web 

Public Interface:
=================
bool fileConverter(std::string folderPath, std::unordered_map<std::string, std::set<std::string>> &dependencyTable, std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& typeTable);	//converts file to .htm file using instream & outstream
void htmlParser(ifstream& inStream, ofstream& outStream, std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& typeTable, std::string fileTOParse);									// Prototype function Html parser replaces tokens < & > with &lt & &gt to convert them to htm line by line		
void htmlParserDiv(ifstream& inStream, ofstream& outStream, std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& typeTable, std::string fileTOParse);									//Html parser replaces tokens < & > with &lt & &gt to convert them to htm
void htmlPageGenerator(ofstream& outStream, string fileName, std::vector<std::string> depFiles);			//Defines logic to generate html page
void ASTParser(ASTNode* pNodeSet);																			//parses AST node and calls GenrateDivHideTable function to generate table
void TokReplacer(string &str, char tok_find, const string str_replace);										//function to replace tokens
bool GenerateDivHideTable(std::string typeName,std::string fileName,int startLineCount,int endLineCount);	//Generates table from AST parser consisting typename,file,startcount,endcount 
std::string addDiv(const std::string& divId);																//Defines start tag for div 
std::string addHideButton(const std::string& divId);														//Adds Expand/Collpase button
std::string endDivTag();																					//Defines End tag of div
void Tokenizer(ofstream& outStream, std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& typeTable,std::string file,int lineCount);   //matches the line count with stat and end count of class/function and adds div start & end Tag
std::string GetFile(const std::string& str);	//gets fileName from Path																																						// to create strong component graph
std::string GenerateIndexer();						//generates random id
std::string DivTagAdder(const std::string& divId);	//adds button and div start tag
std::string GenerateIndexList();					//generate index page

* - Build Process:
=================
Required files
- FileSystem.h,ActionsAndRules.h
Build commands
This is a static library builds with Test Executive
- devenv CodeAnalyzer.sln

* - Maintenance information
=================
ver 1.0 : 04 Apr 2017
ver 1.1 : 04 May 2017		Changed the Relative Path for generating Files
-	second release
*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <set>
#include <unordered_map>
#include "../FileSystem/FileSystem.h"
#include "../Parser/ActionsAndRules.h"

using namespace std;
using namespace CodeAnalysis;

//class that defines the functionalities used to publish web page from .h,.cpp files
class Publisher {
private:
	 FileSystem::Path fileInfo;
	 AbstrSynTree& ASTref_;
	 std::unordered_map<std::string,pair<std::string, pair<int, int>>> divHideTable;
	 std::vector<std::string> indexFileList;

public:
	Publisher();
	bool fileConverter(std::string folderPath, std::unordered_map<std::string, std::set<std::string>> &dependencyTable, std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& typeTable);
	void htmlParser(ifstream& inStream, ofstream& outStream, std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& typeTable, std::string fileTOParse);
	void htmlParserDiv(ifstream& inStream, ofstream& outStream, std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& typeTable, std::string fileTOParse);
	void htmlPageGenerator(ofstream& outStream, string fileName, std::vector<std::string> depFiles);
	void ASTParser(ASTNode* pNodeSet);
	void TokReplacer(string &str, char tok_find, const string str_replace);
	bool GenerateDivHideTable(std::string typeName,std::string fileName,int startLineCount,int endLineCount);
	std::string addDiv(const std::string& divId);
	std::string addHideButton(const std::string& divId);
	std::string endDivTag();
	void Tokenizer(ofstream& outStream, std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& typeTable,std::string file,int lineCount);
	std::string GetFile(const std::string& str);
	std::string GenerateIndexer();
	std::string DivTagAdder(const std::string& divId);
	std::string GenerateIndexList();

};
