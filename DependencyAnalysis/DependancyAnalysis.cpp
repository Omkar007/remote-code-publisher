/////////////////////////////////////////////////////////////////////
// DependencyAnalysis.h -		Test Stub				           //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:   Dependency Analyzer CSE687 Pr2, Mar-7           //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu				   //
/////////////////////////////////////////////////////////////////////
/*
*/

#include "../DependencyAnalysis/DependancyAnalysis.h"

#ifdef depAnalysis
int main() 
{
	DependencyAnalysis dep;
	std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>> typeTable;
	ASTNode* pAstRoot = dep.getFilesFromDir();
	std::vector<std::pair<std::string, std::string>> myVec;
	myVec.push_back(make_pair("class", "C:\\Users\\Omkar-PC\\Desktop\\uploaded\\CodeAnalyzerEx\\Test\\Grandparent.h"));
	typeTable["GrandParent"] = myVec;
	std::unordered_map<std::string, std::set<std::string>> DepTable = analyseFiles.DependencyAnalyzer(ta.accessMyTypeTable(), pAstRoot);
}
#endif