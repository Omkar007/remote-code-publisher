#pragma once
/////////////////////////////////////////////////////////////////////////////////
// ClientSendRecv.h - Exposes Clients Sender and Receiver Logic				   //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017				   //
//																			   //
//  Language:      Visual C++ 2015											   //
//  Platform:      HP Pavilion, Windows 10									   //
//  Application:   Dependency Analyzer CSE687 Pr4, Apr-4					   //
//  Author:        Omkar Patil, CST 4-187, Syracuse University				   //
//                 (315) 949-8810, ospatil@syr.edu							   //	
//////////////////////////////////////////////////////////////////////////////////
/*
* - Manual Information
* This package implements a client that receives HTTP style messages and
* files from server and sends messages & files to server.


Public Interface:
=================
void execute(std::string& category, std::string& files);		//send files to server
void sendPublishMsg(std::string& msgType, std::string& files);	//sends the publish msg to server
void sendGetAllCategoriesFiles(const std::string& categories);	//sends get all files category request to server
void sendDeleteRequest(const std::string& category,const std::string& file);	//sends the delete file request to server
void downloadCSSJSFiles(const std::string& cssjsPath);	//sends the js and css files request to server
void sendNoparentRequestList(const std::string& category);	//sends the noparent list request to server
HttpMessage makeMessage(size_t n, const std::string& msgBody, const EndPoint& ep);  //creates the http messages
void sendMessage(HttpMessage& msg, Socket& socket);	//sends the Message to the server
bool sendFile(const std::string& category,const std::string& fqname, Socket& socket);	//sends the files to the server
std::string GetFile(const std::string& str); //get file from absolute path
HttpMessage CreateDiffMsg(const std::string msgType, const EndPoint& myEndPoint, HttpMessage::Attribute attrib,const std::string& body, const EndPoint& ep); //create msgs on basis on request type

* - Build Process:
=================
Required files
MsgClient.cpp, MsgServer.cpp
*   HttpMessage.h, HttpMessage.cpp
*   Cpp11-BlockingQueue.h
*   Sockets.h, Sockets.cpp
*   FileSystem.h, FileSystem.cpp
*   Logger.h, Logger.cpp
*   Utilities.h, Utilities.cpp
Build commands
Builds as a static library with Test Execuitve
- devenv TestExecuitve.sln

* - Maintenance information
=================
ver 1.0 : 04 Apr 2017
-	first release
*/
#include <iostream>
#include "../HttpMessage/HttpMessage.h"
#include "../Sockets/Sockets.h"

//class the is used as sender for client
class MsgClient
{
public:
	using EndPoint = std::string;
	void execute(std::string& category, std::string& files);
	void sendPublishMsg(std::string& msgType, std::string& files);
	void sendGetAllCategoriesFiles(const std::string& categories);
	void sendDeleteRequest(const std::string& category,const std::string& file);
	void downloadCSSJSFiles(const std::string& cssjsPath);
	void sendNoparentRequestList(const std::string& category);
	MsgClient() {
		while (!si.connect("localhost", 8080))
		{	std::cout<<"\n client waiting to connect";
			::Sleep(100);
		}
	}
	~MsgClient() {
		HttpMessage msg = makeMessage(1, "quit", "toAddr:localhost:8080");
		sendMessage(msg, si);
	}

private:
	SocketSystem ss;
	SocketConnecter si;
	HttpMessage makeMessage(size_t n, const std::string& msgBody, const EndPoint& ep);
	void sendMessage(HttpMessage& msg, Socket& socket);
	bool sendFile(const std::string& category,const std::string& fqname, Socket& socket);
	std::string GetFile(const std::string& str);
	HttpMessage CreateDiffMsg(const std::string msgType, const EndPoint& myEndPoint, HttpMessage::Attribute attrib,const std::string& body, const EndPoint& ep);
};

//class that is used as receiver for client
class ServerHandler {
public:
	ServerHandler(BlockingQueue<HttpMessage>& msgQ) : servReceiverQ(msgQ) {}
	void operator()(Socket socket);
	std::string GetExePath();
	std::string GetRepository();
	std::string GetFile(const std::string& str);
	void processHandler(HttpMessage msg);
private:
	bool connectionClosed_;
	HttpMessage readMessagefromServer(Socket& socket);
	bool readFilefromServer(const std::string& filename, size_t fileSize, Socket& socket);
	HttpMessage MessageParser(HttpMessage& msg, Socket& socket);
	BlockingQueue<HttpMessage>& servReceiverQ;
};