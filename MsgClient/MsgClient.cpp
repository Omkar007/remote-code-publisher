/////////////////////////////////////////////////////////////////////
// MsgClient.cpp -  Client's Sender and Receiver				   //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:    Code Analyzer  CSE687 Pr4, May-4               //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu				   //
/////////////////////////////////////////////////////////////////////
/*
 */
#include "../HttpMessage/HttpMessage.h"
#include "../Sockets/Sockets.h"
#include "../FileSystem/FileSystem.h"
#include "../Logger/Logger.h"
#include "../Utilities/Utilities.h"
#include "ClientSendRecv.h"
#include <string>
#include <iostream>
#include <sstream>
#include <thread>

using namespace Logging;
using Show = StaticLogger<1>;
using namespace Utilities;
using Utils = StringHelper;

//function used to read the messages send by server
HttpMessage ServerHandler::readMessagefromServer(Socket& socket)
{
	connectionClosed_ = false;
	HttpMessage msg;
	while (true)
	{
		std::string attribString = socket.recvString('\n');
		if (attribString.size() > 1)
		{	HttpMessage::Attribute attrib = HttpMessage::parseAttribute(attribString);
			msg.addAttribute(attrib);
		}
		else
		{break;}
	}
	if (msg.attributes().size() == 0)
	{	connectionClosed_ = true;
		return msg;
	}	
	if (msg.attributes()[0].first == "POST")
	{msg=MessageParser(msg,socket);}
	if (msg.attributes()[0].first == "PUBLISH") {
		msg = MessageParser(msg, socket);}
	if (msg.attributes()[0].first == "DELETE") {
		msg = MessageParser(msg, socket);}
	if (msg.attributes()[0].first == "GETCATFILES") {
		msg = MessageParser(msg, socket);}
	if (msg.attributes()[0].first == "DOWNLOADJSCSS") {
		msg = MessageParser(msg, socket);
	}
	if (msg.attributes()[0].first == "NOPARENT") {
		msg = MessageParser(msg, socket);
	}
	return msg;
}

//function used to read the files send by server
bool ServerHandler::readFilefromServer(const std::string & filename, size_t fileSize, Socket & socket)
{
	std::string fqname = GetRepository()+GetFile(filename);
	FileSystem::File file(fqname);
	file.open(FileSystem::File::out, FileSystem::File::binary);
	if (!file.isGood())
	{	Show::write("\n\n  can't open file " + fqname);
		return false;
	}
	const size_t BlockSize = 2048;
	Socket::byte buffer[BlockSize];
	size_t bytesToRead;
	while (true)
	{	if (fileSize > BlockSize)
			bytesToRead = BlockSize;
		else
			bytesToRead = fileSize;

		socket.recv(bytesToRead, buffer);

		FileSystem::Block blk;
		for (size_t i = 0; i < bytesToRead; ++i)
			blk.push_back(buffer[i]);

		file.putBlock(blk);
		if (fileSize < BlockSize)
			break;
		fileSize -= BlockSize;
	}
	file.close();
	return true;
}

//function used to parse the messages 
HttpMessage ServerHandler::MessageParser(HttpMessage & msg, Socket & socket)
{
	std::string filename = msg.findValue("file");
	if (filename != "")
	{
		size_t contentSize;
		std::string sizeString = msg.findValue("content-length");
		if (sizeString != "")
			contentSize = Converter<size_t>::toValue(sizeString);
		else
			return msg;
		readFilefromServer(filename, contentSize, socket);
	}
	if (filename != "")
	{	msg.removeAttribute("content-length");
		std::string bodyString = "<file>" + filename + "</file>";
		std::string sizeString = Converter<size_t>::toString(bodyString.size());
		msg.addAttribute(HttpMessage::Attribute("content-length", sizeString));
		msg.addBody(bodyString);}
	else
	{	size_t numBytes = 0;
		size_t pos = msg.findAttribute("content-length");
		if (pos < msg.attributes().size())
		{
			numBytes = Converter<size_t>::toValue(msg.attributes()[pos].second);
			Socket::byte* buffer = new Socket::byte[numBytes + 1];
			socket.recv(numBytes, buffer);
			buffer[numBytes] = '\0';
			std::string msgBody(buffer);
			msg.addBody(msgBody);
			delete[] buffer;
		}
	}
	return msg;
}

// Used to close the connection at Client End
void ServerHandler::operator()(Socket socket)
{
	while (true)
	{
		HttpMessage msg = readMessagefromServer(socket);
		if (connectionClosed_ || msg.bodyString() == "quit")
		{
			Show::write("\n\n  ServerHandler thread is terminating");
			break;
		}
		servReceiverQ.enQ(msg);
	}
}

//function used for getting all files from repository on category basis
std::string ServerHandler::GetExePath()
{
	char ownPth[MAX_PATH];
	// Will contain exe path
	HMODULE hModule = GetModuleHandle(NULL);
	if (hModule != NULL)
	{	// When passing NULL to GetModuleHandle, it returns handle of exe itself
		GetModuleFileNameA(hModule, ownPth, (sizeof(ownPth)));
	}
	return std::string(ownPth);
}

//Get the relative path of downloaded folder on client's machine on basis of path given by client
std::string ServerHandler::GetRepository()
{
	return FileSystem::Path::fileSpec(FileSystem::Path::getPath(GetExePath()), "..\\..\\DownloadedFiles\\");
}

//function used to get the file name 
std::string ServerHandler::GetFile(const std::string & str)
{
	std::size_t found = str.find_last_of("/\\");
	return str.substr(found + 1);
}

//function used to make messages on request type
HttpMessage MsgClient::makeMessage(size_t n, const std::string& body, const EndPoint& ep)
{ HttpMessage msg;
  HttpMessage::Attribute attrib;
  EndPoint myEndPoint = "localhost:8081";  // ToDo: make this a member of the sender                                 // given to its constructor.
  switch (n)
  {
  case 1:
	  msg=CreateDiffMsg("POST",myEndPoint,attrib,body,ep);
    break;
  case 2:
	  msg=CreateDiffMsg("PUBLISH", myEndPoint, attrib, body, ep);
	  break;
  case 3:
	  msg = CreateDiffMsg("DELETE", myEndPoint, attrib, body, ep);
	  break;
  case 4:
	  msg = CreateDiffMsg("GETCATFILES", myEndPoint, attrib, body, ep);
	  break;
  case 5:
	  msg = CreateDiffMsg("DOWNLOADJSCSS", myEndPoint, attrib, body, ep);
	  break;
  case 6:
	  msg = CreateDiffMsg("NOPARENT", myEndPoint, attrib, body, ep);
	  break;
  default:
    msg.clear();
    msg.addAttribute(HttpMessage::attribute("Error", "unknown message type"));
  }
  return msg;
}

//function used to send the message using socket to server
void MsgClient::sendMessage(HttpMessage& msg, Socket& socket)
{
  std::string msgString = msg.toString();
  socket.send(msgString.size(), (Socket::byte*)msgString.c_str());
}

//function used to send files to the server
bool MsgClient::sendFile(const std::string& category,const std::string& filename, Socket& socket)
{ FileSystem::FileInfo fi(filename);
  size_t fileSize = fi.size();
  std::string sizeString = Converter<size_t>::toString(fileSize);
  FileSystem::File file(filename);
  file.open(FileSystem::File::in, FileSystem::File::binary);
  if (!file.isGood())
    return false;
  HttpMessage msg = makeMessage(1, "", "localhost::8080");
  msg.addAttribute(HttpMessage::Attribute("category", category));
  msg.addAttribute(HttpMessage::Attribute("file", filename));
  msg.addAttribute(HttpMessage::Attribute("content-length", sizeString));
  sendMessage(msg, socket);
  const size_t BlockSize = 2048;
  Socket::byte buffer[BlockSize];
  while (true)
  {
    FileSystem::Block blk = file.getBlock(BlockSize);
    if (blk.size() == 0)
      break;
    for (size_t i = 0; i < blk.size(); ++i)
      buffer[i] = blk[i];
    socket.send(blk.size(), buffer);
    if (!file.isGood())
      break;
  }
  file.close();
  return true;
}

//function that sends the send files request to the server
void MsgClient::execute(std::string& category,std::string& files)
{  try
  {	std::istringstream allFiles(files);
	std::string file;
	while (std::getline(allFiles, file, ' ')) {
		sendFile(category,file, si);}
  }
  catch (std::exception& exc)
  { Show::write("\n  Exeception caught: ");
    std::string exMsg = "\n  " + std::string(exc.what()) + "\n\n";
    Show::write(exMsg);
  }
}

//function that sends the publish file request message to server
void MsgClient::sendPublishMsg(std::string& category, std::string& files)
{
	HttpMessage msg;
	std::string msgBody =files;
	msg = makeMessage(2, msgBody, "localhost:8080");
	msg.addAttribute(HttpMessage::Attribute("category", category));
	sendMessage(msg, si);
}

//function that sends requests to get all files on category basis
void MsgClient::sendGetAllCategoriesFiles(const std::string& categories)
{
	std::string retfile = "";
	HttpMessage msg;
	std::string msgBody =categories;
	msg = makeMessage(4, msgBody, "localhost:8080");
	sendMessage(msg, si);
}

//function that sends the delete request message from client to server
void MsgClient::sendDeleteRequest(const std::string & category, const std::string & file)
{
	HttpMessage msg;
	std::string msgBody = file;
	msg = makeMessage(3, msgBody, "localhost:8080");
	msg.addAttribute(HttpMessage::Attribute("category", category));
	sendMessage(msg, si);
}

//function to download javascript and css files 
void MsgClient::downloadCSSJSFiles(const std::string& cssjsPath)
{
	HttpMessage msg;
	std::string msgBody = "ExpandCollapse.js";
	msg = makeMessage(5, msgBody, "localhost:8080");
	msg.addAttribute(HttpMessage::Attribute("cssjsPath",cssjsPath));
	msg.addAttribute(HttpMessage::Attribute("css", "RemotePublisher.css"));
	sendMessage(msg, si);
}

//function to send request for getting the NoParent List from Server
void MsgClient::sendNoparentRequestList(const std::string & category)
{
	std::string retfile = "";
	HttpMessage msg;
	msg = makeMessage(6, category, "localhost:8080");
	sendMessage(msg, si);
}

//function to get file name from filepath
std::string MsgClient::GetFile(const std::string& str)
{
	std::size_t found = str.find_last_of("/\\");
	return str.substr(found + 1);
}

//function to create different messages
HttpMessage MsgClient::CreateDiffMsg(const std::string msgType, const EndPoint & myEndPoint, HttpMessage::Attribute attrib, const std::string & body, const EndPoint & ep)
{	HttpMessage h1;
	h1.clear();
	h1.addAttribute(HttpMessage::attribute(msgType, "Message"));
	h1.addAttribute(HttpMessage::Attribute("mode", "oneway"));
	h1.addAttribute(HttpMessage::parseAttribute("toAddr:" + ep));
	h1.addAttribute(HttpMessage::parseAttribute("fromAddr:" + myEndPoint));
	h1.addBody(body);
	if (body.size() > 0)
	{	attrib = HttpMessage::attribute("content-length", Converter<size_t>::toString(body.size()));
		h1.addAttribute(attrib);
	}
	return h1;
}

//Equeues the messages from server to send it to mock channel
void ServerHandler::processHandler(HttpMessage msg)
{
	if (msg.attributes()[0].first == "POST")
	{
		std::cout << "\n\n Req-5,6,7,8 Client Receives POST Messsage Acknowledgement of his send files request :\n" + msg.bodyString();
	}
	else if (msg.attributes()[0].first == "PUBLISH") {
		if (msg.findValue("file") != "")
			std::cout << "\n\n Req-3,4,6,7,8,10(bonus lazy download) Client Receives PUBLISH files \n" + msg.bodyString();
		else {
			std::cout << "\n\n Req-3,4,6,7,8  Client Receives PUBLISH files msg Acknowldgement with files: \n" + msg.bodyString();
			std::string indexfile = GetRepository()+ GetFile(msg.bodyString()+".htm");
			while (!FileSystem::File::exists(indexfile)) {
				std::cout << "\n Waiting for htm file...";
				Sleep(100);
			}
			std::string command("start \"\" \"" + indexfile + "\"");
			std::system(command.c_str());
		}
	}
	else if (msg.attributes()[0].first == "DELETE") {
		std::cout << "\n\n Req-6,7,8 Client Receives DELETE files msg Acknowldegment from Server: \n" + msg.bodyString();
	}
	else if (msg.attributes()[0].first == "GETCATFILES") {
		std::cout << "\n\n Req-5,6,7,8 Client Receives ALL FILES for the Requested Category: \n" + msg.bodyString();
	}
	else if (msg.attributes()[0].first == "NOPARENT") {	
		std::cout << "\n\n Req-6,7,8 Client Receives NO Parent File List for the Requested Category: \n" + msg.bodyString();
	}
}

//entry point - runs two clients each on its own thread >------
int main()
{
	::SetConsoleTitle(L"Clients Running on Threads");
	BlockingQueue<HttpMessage> recvQ;
	SocketSystem ss;
	SocketListener sl(8081, Socket::IP6);
	ServerHandler cp(recvQ);
	sl.start(cp);
	std::thread t1;
	while (true)
	{
		HttpMessage msg = recvQ.deQ();
		std::thread t1(
			[&]() {
			cp.processHandler(msg);
		}
		);
		t1.join();
	}
}








