/////////////////////////////////////////////////////////////////////////////////
// MockChannel.cpp - Passes Message request to client & receives response from server //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017				   //
//																			   //
//  Language:      Visual C++ 2015											   //
//  Platform:      HP Pavilion, Windows 10									   //
//  Application:   Dependency Analyzer CSE687 Pr3, Apr-4					   //
//  Author:        Omkar Patil, CST 4-187, Syracuse University				   //
//                 (315) 949-8810, ospatil@syr.edu							   //	
//////////////////////////////////////////////////////////////////////////////////
/*
* - Manual Information
This package sends the message request received from GUI and receives the results from the Server
processes them and display them on GUI

Public Interface:
=================
MockChannel(ISendr* pSendr, IRecvr* pRecvr);	//defines the receiver and sender 
void processMockChannel(std::string& operation, std::string& msg); //process messages from GUI
void start(); //starts the logic and sending and receiving messages

* - Build Process:
=================
Required files
- ClientSendRecv.h,Cpp-BlockingQueue.h,Sockets.h,Utilities.h,HttpMessages.h,Logger.h, Filesystem.h
Build commands
This is a static library builds with CppCli-WPF-App
- devenv  CppCli-WPF-App.sln

* - Maintenance information
=================
ver 1.0 : 04 Apr 2017
-	first release
*/
#define IN_DLL
#include "MockChannel.h"
#include "Cpp11-BlockingQueue.h"
#include "../MsgClient/ClientSendRecv.h"
#include <string>
#include <thread>
#include <iostream>
#include <sstream>

using BQueue = BlockingQueue < Message >;

/////////////////////////////////////////////////////////////////////////////
// Sendr class
// - accepts messages from client for consumption by MockChannel
//
class Sendr : public ISendr
{
public:
  void postMessage(const Message& msg);
  BQueue& queue();
private:
  BQueue sendQ_;
};

void Sendr::postMessage(const Message& msg)
{
  sendQ_.enQ(msg);
}

BQueue& Sendr::queue() { return sendQ_; }

/////////////////////////////////////////////////////////////////////////////
// Recvr class
// - accepts messages from MockChanel for consumption by client
//
class Recvr : public IRecvr
{
public:
  Message getMessage();
  BQueue& queue();
private:
  BQueue recvQ_;
};

Message Recvr::getMessage()
{
  return recvQ_.deQ();
}

BQueue& Recvr::queue()
{
  return recvQ_;
}
/////////////////////////////////////////////////////////////////////////////
// MockChannel class
// - reads messages from Sendr and writes messages to Recvr
//
class MockChannel : public IMockChannel
{
public:
  MockChannel(ISendr* pSendr, IRecvr* pRecvr);
  void processMockChannel(std::string& operation, std::string& msg, BQueue& recvQ);
  MsgClient client;
  void start();
  void stop();
private:
  std::string retFile;
  std::thread thread_;
  ISendr* pISendr_;
  IRecvr* pIRecvr_;
  bool stop_ = false;
};

//----< pass pointers to Sender and Receiver >-------------------------------

MockChannel::MockChannel(ISendr* pSendr, IRecvr* pRecvr) : pISendr_(pSendr), pIRecvr_(pRecvr) {}

//process the request from GUI and sends them to client
void MockChannel::processMockChannel(std::string & operation, std::string & msg, BQueue& recvQ)
{	std::size_t found = msg.find_first_of(" ");

	if (operation=="sendFiles") {
		client.execute(msg.substr(0, found), msg.substr(found + 1));
		recvQ.enQ(msg.substr(found + 1));
	}
	else if(operation=="publishFiles") {
		client.sendPublishMsg(msg.substr(0, found), msg.substr(found + 1));
	}
	else if(operation=="getAllFiles") {
		client.sendGetAllCategoriesFiles(msg.substr(0, found));
	}
	else if(operation=="noParentList") {
		client.sendNoparentRequestList(msg.substr(0, found));
	}
	else if (operation=="deleteFile") {
		client.sendDeleteRequest(msg.substr(0, found),msg.substr(found + 1));
	}
}

//start the process of Sending and Receiving Messages
void MockChannel::start()
{
  std::cout << "\n  MockChannel starting up";
  std::string category;
  thread_ = std::thread(
    [this]() {
    Sendr* pSendr = dynamic_cast<Sendr*>(pISendr_);
    Recvr* pRecvr = dynamic_cast<Recvr*>(pIRecvr_);
    if (pSendr == nullptr || pRecvr == nullptr)
    {std::cout << "\n  failed to start Mock Channel\n\n";
      return; }
    BQueue& sendQ = pSendr->queue();
    BQueue& recvQ = pRecvr->queue();
	client.downloadCSSJSFiles("CssJsFiles");
    while (!stop_)
    { std::cout << "\n  Mock Channel dequeuing message"; 
      Message msg = sendQ.deQ(); 
	
	  std::size_t fnd = msg.find_first_of(";");
	  std::string input = msg.substr(fnd + 1);
	  processMockChannel(msg.substr(0, fnd), input,recvQ);
	//  recvQ.enQ(input);
	}
    std::cout << "\n  Server stopping\n\n";
  });
}
//----< signal server thread to stop >---------------------------------------

void MockChannel::stop() { stop_ = true; }

//----< factory functions >--------------------------------------------------

ISendr* ObjectFactory::createSendr() { return new Sendr; }

IRecvr* ObjectFactory::createRecvr() { return new Recvr; }

//creates the pointers to sender and receiver of Mockchannel
IMockChannel* ObjectFactory::createMockChannel(ISendr* pISendr, IRecvr* pIRecvr) 
{ 
  return new MockChannel(pISendr, pIRecvr); 
}

#ifdef TEST_MOCKCHANNEL

//----< test stub >----------------------------------------------------------
int main()
{
  ObjectFactory objFact;
  ISendr* pSendr = objFact.createSendr();
  IRecvr* pRecvr = objFact.createRecvr();
  IMockChannel* pMockChannel = objFact.createMockChannel(pSendr, pRecvr);

  BlockingQueue<HttpMessage> recvQ;
  SocketSystem ss;

  pMockChannel->start();
  pSendr->postMessage("Hello World");
  pSendr->postMessage("CSE687 - Object Oriented Design");
  Message msg = pRecvr->getMessage();

}
#endif
