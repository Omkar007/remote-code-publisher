#pragma once
/////////////////////////////////////////////////////////////////////
// DependencyAnalysis.h - Analalyzes dependencies and creates dependency table   //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:   Dependency Analyzer CSE687 Pr2, Mar-7           //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu				   //	
/////////////////////////////////////////////////////////////////////
/*
* - Manual Information
This package analyzes the dependencies among the files and creates the dependency table

Public Interface:
=================
std::unordered_map<std::string, std::set<std::string>> DependencyAnalyzer(std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& my_typeTable, ASTNode* pNode);			//Used to store the dependency table
void displayDepencyTable(std::unordered_map<std::string, std::set<std::string>> & dept_Table);																										//Used to display the dependency table
std::unordered_map<std::string, std::set<std::string>>& getDependencyFilesMap() { return dependencyTable; }																							//getter method to return dependency table
void DependencyAnalysis::displayDB();																																								//Used to display Nosql db contents
void getFilesFromDir(std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& my_typeTable);																				// To get the files from directories recursively
void StrongComponentRequirement();																																									// to create strong component graph

* - Build Process:
Required files
- DependencyAnalysis.h,DependencyAnalysis.cpp
Build commands
This is a static library builds with code analyzer
- devenv CodeAnalyzer.sln

* - Maintenance information
ver 1.1 04 May 2017	
- Removed warning for signed value
ver 1.0 : 07 Mar 2017
-	first release
*/

#include "../Tokenizer/Tokenizer.h"
#include "../Utilities/Utilities.h"
#include "../TypeTable/TypeTable.h"
#include "../Parser/ActionsAndRules.h"
#include "../FileSystem/FileSystem.h"
#include <set>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace Scanner;
using namespace Logging;
using namespace CodeAnalysis;

using Key = std::string;
using StrData = std::string;
using Keys = std::vector<std::string>;
class DependencyAnalysis {

public:
	DependencyAnalysis();
	std::unordered_map<std::string, std::set<std::string>> DependencyAnalyzer(std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& my_typeTable, ASTNode* pNode);
	void displayDepencyTable();
	std::unordered_map<std::string, std::set<std::string>>& getDependencyFilesMap() { return dependencyTable; }
	ASTNode* getFilesFromDir();
	void fillDepTable(std::string files,std::string tokens, std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& my_typeTable);
	std::unordered_map<std::string, int> graph_map;
	
private:
	std::unordered_map<std::string, std::set<std::string>> dependencyTable;
	AbstrSynTree& ASTref_;
	FileSystem::Directory directories;
	FileSystem::Path extensions;
};

inline DependencyAnalysis::DependencyAnalysis() :ASTref_(Repository::getInstance()->AST()) {}

//Function to insert the dependency analysis in the dependency analysis table
inline std::unordered_map<std::string, std::set<std::string>> DependencyAnalysis::DependencyAnalyzer(std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& my_typeTable, ASTNode* pNode)
{
	std::string files;
	static std::string path = "";
	try {files = pNode->path_;
		std::ifstream in(files);
		if ((!in.good()) && pNode->path_ == path) {}
		else
		{	Toker toker;
			toker.returnComments();
			toker.attach(&in);
			do
			{
				std::string tokens = toker.getTok();
				fillDepTable(files,tokens,my_typeTable);
			} while (in.good());
		}
		in.close();
		for (auto pChild : pNode->children_)
			DependencyAnalyzer(my_typeTable, pChild);
	}
	catch (std::logic_error& ex)
	{std::cout << "\n  " << ex.what();
	}
	return dependencyTable;
}

//Function to display the dependency analysis table
inline void DependencyAnalysis::displayDepencyTable()
{
	std::cout << "\n\n Requirement-5 Shall provide a DependencyAnalysis package that identifies all of the dependencies between files in a specified file collection.\n\n";
	int length1 = 50, length2 = 50;
	std::set<std::string>::iterator it_in;
	for (auto it : dependencyTable) {
		std::cout<<"\n\n" << std::setw(length1) << std::left << it.first;
		std::cout << "Dependencies are :";
		std::cout << " ";
		for (it_in = it.second.begin(); it_in != it.second.end(); ++it_in) {
			std::cout << *it_in;
		}
	}
}

//Function to generate the package dependency information using the ASTNode
inline 	ASTNode* DependencyAnalysis::getFilesFromDir()
{
	ASTNode* pRoot = ASTref_.root();
	return pRoot;
}


inline void DependencyAnalysis::fillDepTable(std::string files,std::string tokens,std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& my_typeTable)
{
	for (auto typetable : my_typeTable)
	{
		for (int i = 0; i<(signed)typetable.second.size(); i++)
		{
			if (typetable.first == tokens)
			{
				if (files != typetable.second[i].second)
				{
					dependencyTable[files];
					dependencyTable[files].insert(typetable.second[i].second);
				}
				//else { dependencyTable[files]; }
			}
		}
	}
}


