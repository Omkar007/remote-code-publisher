/////////////////////////////////////////////////////////////////////
// TestExecutive.cpp -	Remote Publisher to call the Code Publisher functionality//
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:   Code Analyzer  CSE687 Pr3, Apr-4	               //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu				   //
/////////////////////////////////////////////////////////////////////

#include "../Publisher/Publisher.h"
#include "../DependencyAnalysis/DependancyAnalysis.h"
#include "../TypeTable/TypeTable.h"
#include "../Analyzer/Executive.h"
#include <iostream>
#include "RemotePublisher.h"

std::unordered_map<std::string, std::set<std::string>> CodePublisher::generateHTM(char *directory)
{
	char* argv[] = { "DummyExe", directory,"*.h","*.cpp" };
	using namespace CodeAnalysis;
	CodeAnalysisExecutive exec;
	bool succeeded = exec.ProcessCommandLine(4, argv);
	exec.getSourceFiles();
	exec.processSourceCode(false);
	TypeAnal1 ta;
	ta.doTypeAnal();
	DependencyAnalysis analyseFiles;
	Publisher publisher;
	ASTNode* pAstRoot = analyseFiles.getFilesFromDir();
	std::unordered_map<std::string, std::set<std::string>> htmlDepTable = analyseFiles.DependencyAnalyzer(ta.accessMyTypeTable(), pAstRoot);
	publisher.ASTParser(pAstRoot);
	analyseFiles.displayDepencyTable();
	std::string categoryPath(directory);
	bool flag = publisher.fileConverter(categoryPath+"/", htmlDepTable, ta.accessMyTypeTable());
	if (!flag) { std::cout << "HTM file converter failed to open file"; }
	return htmlDepTable;
}


#ifdef publisherCode
int main() {
	CodePublisher cp;
	std::string category = "../Test";
	char *cat = new char[category.length() + 1];
	cp.generateHTM(const_cast<char *>(category.c_str()));
}
#endif // DEBUG
