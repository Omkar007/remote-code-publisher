#pragma once
/////////////////////////////////////////////////////////////////////
// TypeTable.h -		To generate and Analyze type table		   //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:   Dependency Analyzer CSE687 Pr4, 04 May-17       //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu				   //
/////////////////////////////////////////////////////////////////////
/*

Manual Information
=================
This package provides the

Public Interface:
=================
void DFS(ASTNode* pNode);				//Recursively traversing tree by calling DFS
bool doDisplay1(ASTNode* pNode)			//Displaying ASTNode tree
doTypeAnal()							//function to do type analysis

* - Build Process:
=================
Required files
- TypeTable.h, ActionsAndRules.h,TypeAnalysis.cpp
Build commands (either one)
- devenv CodeAnalyzerEx.sln

* - Maintenance information
=================
ver 1.1 : 04 May 2017
- removed warning for signed value
ver 1.0 : 07 Feb 2017
-	first release
*/
#include "../Parser/ActionsAndRules.h"
#include"../FileSystem/FileSystem.h"
#pragma warning (disable : 4101)  // disable warning re unused variable x, below

namespace CodeAnalysis
{
	class TypeAnal1
	{
	public:
		using SPtr = std::shared_ptr<ASTNode*>;

		TypeAnal1();
		std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& accessMyTypeTable() { return my_typeTable; }
		void doTypeAnal();
	private:
		void DFS(ASTNode* pNode);
		AbstrSynTree& ASTref_;
		ScopeStack<ASTNode*> scopeStack_;
		Scanner::Toker& toker_;
		std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>> my_typeTable;
		FileSystem::Path filePath;
	};

	inline TypeAnal1::TypeAnal1() :
		ASTref_(Repository::getInstance()->AST()),
		scopeStack_(Repository::getInstance()->scopeStack()),
		toker_(*(Repository::getInstance()->Toker()))
	{
		std::function<void()> test = [] { int x; };  // This is here to test detection of lambdas.
	}                                              // It doesn't do anything useful for dep anal.

	//To return the types from the ASTNode
	inline bool doDisplay(ASTNode* pNode)
	{
		static std::string toDisplay[] = {
			"function", "lambda", "class", "struct", "enum", "alias", "typedef","namespace"
		};
		for (std::string type : toDisplay)
		{
			if (pNode->type_ == type)
				return true;
		}
		return false;
	}

	//Traversing the AST node recursively to push the data into the type table
	inline void TypeAnal1::DFS(ASTNode* pNode)
	{
		std::string name = "";
		std::string path = "";
		std::string type = "";
		if (pNode->path_ != path)
		{
			name = pNode->name_;
			path = pNode->path_;
			type = pNode->type_;
			std::vector<std::pair<std::string, std::string>> keyResults;
			if ((pNode->type_ == "class") || (pNode->type_ == "struct") || (pNode->type_ == "using") || (pNode->type_ == "enum") || (pNode->type_ == "function") || (pNode->type_ == "alias") || (pNode->type_ == "typedef")) {
				keyResults.push_back(std::make_pair(type, path));
				my_typeTable.insert(std::make_pair(name, keyResults));
			}}
		if (doDisplay(pNode))
		{	std::cout << "\n  " << pNode->name_;
			std::cout << ", " << pNode->type_;
		}
		//loop traversed to get the typedefs
		for (auto it : pNode->decl_) {
			if (it.declType_ == dataDecl && it.pTc->show().substr(0, 7) == "typedef") {
				const Scanner::ITokCollection* pTc = it.pTc;
				std::string n1 = (*pTc)[pTc->find(";") - 1];
			std::vector<std::pair<std::string, std::string>> vec;
			std::string fileName = filePath.getFullFileSpec(it.package_);
				vec.push_back(std::make_pair("typedef",fileName));
				my_typeTable[n1] = vec;
			}
		}	//loop traversed to get the using
		for (auto it : pNode->decl_) {
			if (it.declType_ == usingDecl && it.pTc->show().substr(0, 5) == "using" && pNode->type_!="namespace") {
				const Scanner::ITokCollection* pTc = it.pTc;
				std::string name = (*pTc)[pTc->find("=") - 1];
				std::vector<std::pair<std::string, std::string>> a;
				std::string fileName = filePath.getFullFileSpec(it.package_);
				a.push_back(std::make_pair("using", fileName));
				my_typeTable[name] = a;
			}}
		for (auto pChild : pNode->children_)
			DFS(pChild);
	}

	//Function to display the TypeTable
	inline void TypeAnal1::doTypeAnal()
	{
		int length1 = 20, length2 = 20, length3 = 20;
		std::cout << "\n  starting type analysis:\n";
		std::cout << "\n  scanning AST and displaying important things:";
		std::cout << "\n -----------------------------------------------";
		ASTNode* pRoot = ASTref_.root();
		DFS(pRoot);
		std::cout << "\n\n ===========================TypeTable=============================\n";
		std::cout << "\n--Name-----------------------TypeName----------------------FilePath--------\n";
		std::vector<std::pair<std::string, std::string>> values;
		for (auto it : my_typeTable)
		{
			std::cout<< std::setw(length1);
			std::cout << it.first << std::left << std::setw(length1);
			for (int i = 0; i<it.second.size(); i++)
				std::cout<<std::setw(length1)<<std::left  << it.second[i].first <<"  "<< std::setw(length2) << std::left << it.second[i].second <<std::endl;
		}
		std::cout << std::endl;
	}
}