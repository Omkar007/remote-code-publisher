/////////////////////////////////////////////////////////////////////
// Publisher.cpp -  Code Publisher for Generating HTML Pages	   //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:    Code Analyzer  CSE687 Pr4, May-4               //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu				   //
/////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cstdlib>
#include <set>
#include <unordered_map>
#include <fstream>
#include <sstream>
#include "Publisher.h"

#ifdef PubLish
int main()
{
	ifstream fin;
	ofstream fout;
	std::string folderPath = "../Test";
	std::unordered_map<std::string, std::set<std::string>> dependencyTable;
	std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>> typeTable;
	std::set<std::string> mySet;
	mySet.insert("C:\\Users\\Omkar-PC\\Desktop\\uploaded\\CodeAnalyzerEx\\Test\\Child.h");
	mySet.insert("C:\\Users\\Omkar-PC\\Desktop\\uploaded\\CodeAnalyzerEx\\Test\\parent.h");
	mySet.insert("C:\\Users\\Omkar-PC\\Desktop\\uploaded\\CodeAnalyzerEx\\Test\\Test.cpp");
	dependencyTable["C:\\Users\\Omkar-PC\\Desktop\\uploaded\\CodeAnalyzerEx\\Test\\Grandparent.h"] = mySet;
	
	std::vector<std::pair<std::string, std::string>> myVec;
	myVec.push_back(make_pair("class", "C:\\Users\\Omkar-PC\\Desktop\\uploaded\\CodeAnalyzerEx\\Test\\Grandparent.h"));
	typeTable["GrandParent"]=myVec;
	Publisher publish;
	publish.fileConverter(folderPath,dependencyTable,typeTable);
	cin.get();
	return 0;
}
#endif

Publisher::Publisher() :ASTref_(Repository::getInstance()->AST()) {}

//function that parses files to html files by calling parser & generator method 
bool Publisher::fileConverter(std::string folderPath, std::unordered_map<std::string, std::set<std::string>>& dependencyTable, std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& typeTable)
{
	ifstream fin;
	ofstream fout;
	ASTNode* pRoot = ASTref_.root();
	ASTParser(pRoot);
	std::set<std::string>::iterator it_in;
	for (auto it : dependencyTable) {
		fin.open(it.first);
		if (fin.fail())
		{return false;}
		fout.open(folderPath+ GetFile(it.first)+".htm");
		if (fout.fail())
		{return false;}
		std::vector<std::string> depFiles;
		for (it_in = it.second.begin(); it_in != it.second.end(); ++it_in) {
			depFiles.push_back(*it_in);
		}
		htmlPageGenerator(fout, it.first, depFiles);
		htmlParser(fin,fout,typeTable,it.first);
		indexFileList.push_back(GetFile(it.first) + ".htm");
	}
	return true;
}

// Html parser Prototype for expand/collapse using Typetable & AST Parser 
inline void Publisher::htmlParser(ifstream & inStream, ofstream & outStream,std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& typeTable,std::string fileTOParse)
{
	string next;
	int lineCount = 1;
	while (getline(inStream, next))
	{
		Tokenizer(outStream,typeTable,fileTOParse,lineCount);
		TokReplacer(next, '<', "&lt;");
		TokReplacer(next, '>', "&gt;");
		outStream << next << "\n";
		lineCount++;
	}
	outStream << "</PRE>" << endl;
	outStream << "</body>" << endl;
	outStream << "</html>";
	inStream.close();
	outStream.close();
}

//Function that parses char by char & expand/collapse logic
void Publisher::htmlParserDiv(ifstream & inStream, ofstream & outStream, std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& typeTable, std::string fileTOParse)
{
	char next;
	while (inStream.get(next))
	{
		if (next == '<')
		{
			outStream << "&lt;";
		}
		else if (next == '>')
		{
			outStream << "&gt;";
		}
		else
			outStream << next;

		if (next == '{')
		{
			std::string divId = GenerateIndexer();
			outStream << DivTagAdder(divId);

		}
		if (next == '}') {
			outStream << endDivTag();
		}
	}
	outStream << "</PRE>" << endl;
	outStream << "</body>" << endl;
	outStream << "</html>";
	inStream.close();
	outStream.close();
}
//function that adds html tags and adds css & js to files 
inline void Publisher::htmlPageGenerator(ofstream & fout, string fileName, std::vector<std::string> depFiles)
{
	fout << "<html>" << endl;
	fout << "<head>" << endl;
	fout << "<a href='ExpandCollapse.js'>" << "ExpandCollapse.js" << "</a>" << endl;
	fout << "<a href='RemotePublisher.css'>" << "RemotePublisher.css" << "</a>" << endl;
	fout << "<script src='ExpandCollapse.js'>" << "</script>" << endl;
	fout << "<link rel='stylesheet'" << " " << "href = 'RemotePublisher.css' >" << endl;
	fout << "</head>" << endl;
	fout << "<body>" << endl; 
	fout << "<h3>" << GetFile(fileName) << "</h3>" << endl;
	fout << "<hr/>" << endl;
	fout << "<div class='indent'>" << endl;
	fout << "<h4> Dependencies: </h4>" << endl;
	for (std::vector<std::string>::iterator it = depFiles.begin(); it != depFiles.end(); ++it)
	{
		string htmDepFile = *it + ".htm";
		fout << "<a href=" << GetFile(htmDepFile) << ">" <<GetFile(*it) << "</a>" << endl;
	}
	fout << "</div>" << endl;
	fout << "<hr/>" << endl;
	fout << "<PRE>" << endl;
}

//Function Prototype to Parse AST for detecting class, functions and global functions in the file & generate Expand/Collapse table
void Publisher::ASTParser(ASTNode * pNode)
{
	if (pNode->type_ == "function" || pNode->type_ == "class") {
		GenerateDivHideTable(pNode->name_,pNode->path_,(int)pNode->startLineCount_, (int)pNode->endLineCount_);
	}
	if (pNode->type_ == "namespace")
	{
		for (auto pChild : pNode->children_)
		{
			if (pChild->type_ == "function")
			{
				GenerateDivHideTable(pNode->name_, pNode->path_, (int)pNode->startLineCount_, (int)pNode->endLineCount_);
			}
		}
	}
	for (auto pChild : pNode->children_)
		ASTParser(pChild);
}

//function to replace tokens of stream with specific string
void Publisher::TokReplacer(string & str, char tok_find, const string str_replace)
{
	size_t val;
	while ((val = str.find(tok_find)) != string::npos)
		str.replace(val, 1, str_replace);
}

// function that generates table for Expand and Collapse div
bool Publisher::GenerateDivHideTable(std::string typeName, std::string fileName,int startLineCount, int endLineCount)
{
	if (divHideTable.find(typeName) != divHideTable.end())
		return false;
	divHideTable[typeName] = std::make_pair(fileName, std::make_pair(startLineCount, endLineCount));
		return true;
	
}

//function that generates div
std::string Publisher::addDiv(const std::string & divId)
{
	std::string divStartTag= "<div id = \"" + divId + "\">\n";
	return divStartTag;
}

//function to add expand collapse button
std::string Publisher::addHideButton(const std::string & divId)
{
	std::string button= "<input type=\"button\" onclick = \"expandCollapse('" + divId + "')\" value = \"-\" id=\"" + "ID" + divId + "\"/>\n";
	return button;
}

//function to add div end tag
std::string Publisher::endDivTag()
{
	std::string endDivTag= "</div>\n";
	return endDivTag;
}

//function to append the button/div in the outstream while parsing file to convert it into html file (AST Parser logic Prototype for Expand & Collapse)
void Publisher::Tokenizer(ofstream & outStream, std::unordered_map<std::string, std::vector<std::pair<std::string, std::string>>>& typeTable, std::string fileTOParse,int lineCount)
{
	for (auto it = divHideTable.begin(); it != divHideTable.end(); ++it)
	{
		if (fileTOParse == it->second.first) {
			for (auto typeItr = typeTable.begin(); typeItr != typeTable.end(); ++typeItr) {
				if (it->first == typeItr->first && it->second.second.first == lineCount ) {
					outStream << DivTagAdder(it->first);
				}
				if (it->first == typeItr->first && it->second.second.second == lineCount) {
					outStream << endDivTag();
				}
			}
		}
	}
}

//function to get file name from filepath
std::string Publisher::GetFile(const std::string& str)
{
	std::size_t found = str.find_last_of("/\\");
	return str.substr(found + 1);
}

//Generates random div ID
std::string Publisher::GenerateIndexer()
{
	auto randchar = []() -> char
	{
		const char charset[] =
			"0123456789"
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			"abcdefghijklmnopqrstuvwxyz";
		const size_t max_index = (sizeof(charset) - 1);
		return charset[rand() % max_index];
	};
	std::string str(9, 0);
	std::generate_n(str.begin(), 9, randchar);
	return str;
}

//Adds div to the outstream
std::string Publisher::DivTagAdder(const std::string & divId)
{
	std::string buttDiv =addHideButton(divId) + addDiv(divId);
		return buttDiv;
}

//function to generate Index Page
std::string Publisher::GenerateIndexList()
{
	ofstream fout;
	std::string indexFile = "index.htm";
	fout.open(indexFile);
	fout << "<html>" << endl;
	fout << "<head>" << endl;
	fout << "<a href='ExpandCollapse.js'>" << "ExpandCollapse.js" << "</a>" << endl;
	fout << "<a href='RemotePublisher.css'>" << "RemotePublisher.css" << "</a>" << endl;
	fout << "<script src='ExpandCollapse.js'>" << "</script>" << endl;
	fout << "<link rel='stylesheet'" << " " << "href = 'RemotePublisher.css' >" << endl;
	fout << "</head>" << endl;
	fout << "<body>" << endl;
	fout << "<h3>" << "Index.htm" << "</h3>" << endl;
	fout << "<hr/>" << endl;
	fout << "<div class='indent'>" << endl;
	fout << "<h4> List of Files: </h4>" << endl;
	for (std::vector<std::string>::iterator it = indexFileList.begin(); it != indexFileList.end(); ++it)
	{	string htmDepFile = *it;
		fout << "<a href=" << htmDepFile << ">" << htmDepFile << "</a>"<< "\r\n";
	}
	fout << "<PRE>" << endl;
	fout << "</PRE>" << endl;
	fout << "</body>" << endl;
	fout << "</html>";
	fout.close();
	return indexFile;
}







