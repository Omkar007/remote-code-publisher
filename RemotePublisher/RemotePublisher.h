#pragma once
/////////////////////////////////////////////////////////////////////////////////
// RemoteCodePublisher.h  Package used to call the Code Publisher on Server	   //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017				   //
//																			   //
//  Language:      Visual C++ 2015											   //
//  Platform:      HP Pavilion, Windows 10									   //
//  Application:   Dependency Analyzer CSE687 Pr4, Apr-4					   //
//  Author:        Omkar Patil, CST 4-187, Syracuse University				   //
//                 (315) 949-8810, ospatil@syr.edu							   //	
//////////////////////////////////////////////////////////////////////////////////
/*
* - Manual Information
* This package acts as layer to call the remote code publisher 

Public Interface:
=================
std::unordered_map<std::string, std::set<std::string>> generateHTM(char* directory);	//calls the code publisher 

* - Build Process:
=================
Required files
RemoteCodePublisher.cpp

Build commands
Builds as static library with MsgServer
- devenv MsgServer.sln

* - Maintenance information
=================
ver 1.0 : 04 Apr 2017
-	first release
*/
#include <set>
#include <iostream>

class CodePublisher {
public:
	std::unordered_map<std::string, std::set<std::string>> generateHTM(char* directory);
};