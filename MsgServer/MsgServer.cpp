/////////////////////////////////////////////////////////////////////
// MsgServer.cpp -  Server's Sender and Receiver				   //
//  Omkar Patil, CSE687 - Object Oriented Design, Spring 2017	   //
//																   //
//  Language:      Visual C++ 2015		                           //
//  Platform:      HP Pavilion, Windows 10						   //
//  Application:    Code Analyzer  CSE687 Pr4, May-4               //
//  Author:        Omkar Patil, CST 4-187, Syracuse University     //
//                 (315) 949-8810, ospatil@syr.edu				   //
/////////////////////////////////////////////////////////////////////
/*
*/

#include "../MsgServer/ServerSenderReceiver.h"
#include "../FileSystem/FileSystem.h"
#include "../Logger/Cpp11-BlockingQueue.h"
#include "../Logger/Logger.h"
#include "../Utilities/Utilities.h"
#include <string>
#include <iostream>
#include <set>

using namespace Logging;
using Show = StaticLogger<1>;
using namespace Utilities;

//Make Message to send it to Client End
HttpMessage ServerSender::makeMessageforClient(size_t n, const std::string& body, const EndPoint& ep)
{
	HttpMessage msg;
	HttpMessage::Attribute attrib;
	EndPoint myEndPoint = "localhost:8080";  // ToDo: make this a member of the sender
											 // given to its constructor.
	switch (n)
	{
	case 1:
		msg = CreateDiffMsg("POST", myEndPoint, attrib, body, ep);
		break;
	case 2:
		msg = CreateDiffMsg("PUBLISH", myEndPoint, attrib, body, ep);
		break;
	case 3:
		msg = CreateDiffMsg("DELETE", myEndPoint, attrib, body, ep);
		break;
	case 4:
		msg = CreateDiffMsg("GETCATFILES", myEndPoint, attrib, body, ep);
		break;
	case 5:
		msg = CreateDiffMsg("DOWNLOADJSCSS", myEndPoint, attrib, body, ep);
		break;
	case 6:
		msg = CreateDiffMsg("NOPARENT", myEndPoint, attrib, body, ep);
		break;
	default:
		msg.clear();
		msg.addAttribute(HttpMessage::attribute("Error", "unknown message type"));
	}
	return msg;
}

//----< send message using socket to client >---------------------------------
void ServerSender::sendMessageforClient(HttpMessage& msg, Socket& socket)
{
	std::string msgString = msg.toString();
	socket.send(msgString.size(), (Socket::byte*)msgString.c_str());
}

//Stream and Send files to clients
bool ServerSender::sendFileforClient(const std::string& category,const std::string& filename, Socket& socket)
{	std::string fqname = GetRepository(category) + "\\"+ GetFile(filename);
	std::cout << "\n files to send \n" << fqname;
	FileSystem::FileInfo fi(fqname);
	size_t fileSize = fi.size();
	std::string sizeString = Converter<size_t>::toString(fileSize);
	FileSystem::File file(fqname);
	file.open(FileSystem::File::in, FileSystem::File::binary);
	if (!file.isGood())
		return false;
	HttpMessage msg = makeMessageforClient(2, "", "localhost::8081");
	msg.addAttribute(HttpMessage::Attribute("file", filename));
	msg.addAttribute(HttpMessage::Attribute("category", category));
	msg.addAttribute(HttpMessage::Attribute("content-length", sizeString));
	sendMessageforClient(msg, socket);
	const size_t BlockSize = 2048;
	Socket::byte buffer[BlockSize];
	while (true)
	{
		FileSystem::Block blk = file.getBlock(BlockSize);
		if (blk.size() == 0)
			break;
		for (size_t i = 0; i < blk.size(); ++i)
			buffer[i] = blk[i];
		socket.send(blk.size(), buffer);
		if (!file.isGood())
			break;
	}
	file.close();
	return true;
}

//function to send Publishfiles Acknowledgement to client
void ServerSender::sendPublishFileAcknowledgemnt(const std::string& msgBody)
{
	SocketSystem ss;
	SocketConnecter si;
	while (!si.connect("localhost", 8081))
	{
		std::cout << "\n Server waiting to connect";
		::Sleep(100);
	}
	HttpMessage msg = makeMessageforClient(2, msgBody, "localhost:8081");
	sendMessageforClient(msg, si);
	msg.addAttribute(HttpMessage::Attribute("filetoPublish", msgBody));
	HttpMessage	msg1 = makeMessageforClient(1, "quit", "toAddr:localhost:8081");
	sendMessageforClient(msg1, si);
}

//function to send the js and css files to client
void ServerSender::downloadCSSJSS(HttpMessage& msg)
{
	try {
		std::string cssfile = msg.findValue("css");
		std::string folderPath = msg.findValue("cssjsPath");
		SocketSystem ss;
		SocketConnecter si;
		while (!si.connect("localhost", 8081))
		{
			std::cout << "\n Server waiting to connect"; ::Sleep(100);
		}
			sendFileforClient(folderPath, cssfile, si);
			sendFileforClient(folderPath, msg.bodyString(), si);
	}
	catch (std::exception& exc)
	{
		std::string exMsg = "\n  " + std::string(exc.what()) + "\n\n";
		std::cout << exMsg;
	}
}

//Processing the NoParent List generation from category list and dependency table
std::set<std::string> ServerSender::sendNoParentAckList(const std::string & category)
{
	std::set<std::string>noParentList;
	std::vector<std::string>files = FileSystem::Directory::getFiles(GetRepository(category), "*.cpp");
	std::vector<std::string>files1 = FileSystem::Directory::getFiles(GetRepository(category), "*.h");
	for (int j = 0; j < files1.size();j++) {
		files.push_back(files1[j]);}
	std::set<std::string>::iterator it_in;
		for (auto it : htmlDepTable) {
			for (it_in = it.second.begin(); it_in != it.second.end(); ++it_in) {
				for (int k = 0; k < files.size(); k++) {
				std::string f = GetFile(it.first);
				if (files[k] == GetFile(it.first) || files[k] == GetFile(*it_in)) {}
				else{ noParentList.insert(files[k]); }
			}
		}
	}
	return noParentList;
}

//Sending the Acknowledgement msg of no parent list to client
void ServerSender::parentListAcknowledgement(const std::string & category)
{
	SocketSystem ss;
	SocketConnecter si;
	std::set<std::string>::iterator it_in;
	while (!si.connect("localhost", 8081))
	{
		std::cout << "\n Server waiting to connect";
		::Sleep(100);
	}
	std::string npFiles = "";
	std::set<std::string> files=sendNoParentAckList(category);
	for (it_in = files.begin(); it_in != files.end(); ++it_in) {
		npFiles = *it_in + "," + npFiles;
	}
	std::size_t found = npFiles.find_last_of(",");
	npFiles.substr(0,found-1);
	HttpMessage msg = makeMessageforClient(6, npFiles, "localhost:8081");
	sendMessageforClient(msg, si);
	HttpMessage	msg1 = makeMessageforClient(1, "quit", "toAddr:localhost:8081");
	sendMessageforClient(msg1, si);
}
//function to send the send Files Acknowledgment reply to client
void ServerSender::sendFilesReply(const std::string& msgBody)
{
	SocketSystem ss;
	SocketConnecter si;
	while (!si.connect("localhost", 8081))
	{
		std::cout << "\n Server waiting to connect";
		::Sleep(100);
	}
	HttpMessage msg = makeMessageforClient(1, msgBody, "localhost:8081");
	sendMessageforClient(msg, si);
	HttpMessage	msg1 = makeMessageforClient(1, "quit", "toAddr:localhost:8081");
	sendMessageforClient(msg1, si);
}

//function to send all files acknowledgement to client
void ServerSender::sendAllFilesAcknowldement(const std::string& msgBody)
{
	SocketSystem ss;
	SocketConnecter si;
	while (!si.connect("localhost", 8081))
	{
		std::cout << "\n Server waiting to connect";
		::Sleep(100);
	}
	HttpMessage msg = makeMessageforClient(4, msgBody, "localhost:8081");
	sendMessageforClient(msg, si);
	HttpMessage	msg1 = makeMessageforClient(1, "quit", "toAddr:localhost:8081");
	sendMessageforClient(msg1, si);
}

//Sender Method for Server to send messages to client
void ServerSender::sendFileforPublishRequest(const std::string& category,const std::string& msg)
{	try{SocketSystem ss;
	SocketConnecter si;
	std::vector<std::string>htmlfiles;
	htmlDepTable=publish.generateHTM(const_cast<char *>(GetRepository(category).c_str()));
	while (!si.connect("localhost", 8081))
	{	std::cout << "\n Server waiting to connect";::Sleep(100);}
		htmlfiles = AnalyzeFileDependencies(msg,htmlDepTable);
	for (size_t i = 0; i < htmlfiles.size(); ++i)
		{
		sendFileforClient(category, htmlfiles[i], si);
		}
	sendPublishFileAcknowledgemnt(msg);
	}
	catch (std::exception& exc)
	{
		std::string exMsg = "\n  " + std::string(exc.what()) + "\n\n";
		std::cout << exMsg;
	}
}

//function to get file name from filepath
std::string ServerSender::GetFile(const std::string& str)
{
	std::size_t found = str.find_last_of("/\\");
	return str.substr(found + 1);
}

//function to delete file from Server on basis of category
bool ServerSender::ProcessDeleteFileRequest(const std::string& category, const std::string & file)
{
	bool flag = false;
	std::string deleteFile =GetRepository(category) +"\\"+ GetFile(file);
	if (remove(deleteFile.c_str())==0)
		ProcessDeleteFileRequest(file,true);
	return flag;
}


//function that sends the delete Request Acknowledgement to the client 
void ServerSender::ProcessDeleteFileRequest(const std::string& file,bool flag)
{
	std::string message = "";
	SocketSystem ss;
	SocketConnecter si;
	while (!si.connect("localhost", 8081))
	{
		std::cout << "\n Server waiting to connect";
		::Sleep(100);
	}
	if (flag)
		message = "file deleted successfully";
	else
		message = "error in deleting file";
	HttpMessage msg = makeMessageforClient(3, message, "localhost:8081");
	sendMessageforClient(msg, si);
	HttpMessage	msg1 = makeMessageforClient(1, "quit", "toAddr:localhost:8081");
	sendMessageforClient(msg1, si);

}


//function that reads Messages from Client on basis of msg type >------------------
HttpMessage ClientHandler::readMessage(Socket& socket)
{
  connectionClosed_ = false;
  HttpMessage msg;
  while (true)
  {	 std::string attribString = socket.recvString('\n');
    if (attribString.size() > 1)
    { HttpMessage::Attribute attrib = HttpMessage::parseAttribute(attribString);
      msg.addAttribute(attrib);
    }
    else {break;}
  }
  if (msg.attributes().size() == 0)
  {connectionClosed_ = true;
	return msg; 
  }
  if (msg.attributes()[0].first == "POST") {
	  msg = MessageParser(msg, socket);}
  if (msg.attributes()[0].first == "PUBLISH") {
	  msg = MessageParser(msg, socket);}
  if (msg.attributes()[0].first == "DELETE") {
	  msg = MessageParser(msg, socket);}
  if (msg.attributes()[0].first == "GETCATFILES") {
	  msg = MessageParser(msg, socket);}
  if (msg.attributes()[0].first == "DOWNLOADJSCSS") {
	  msg = MessageParser(msg, socket);
  }
  if (msg.attributes()[0].first == "NOPARENT") {
	  msg = MessageParser(msg, socket);
  }
  return msg;
}

//function to parse Message for file or message
HttpMessage ClientHandler::MessageParser(HttpMessage & msg, Socket & socket)
{
	std::string category = msg.findValue("category");
	std::string filename = msg.findValue("file");
	if (filename != "")
	{
		size_t contentSize;
		std::string sizeString = msg.findValue("content-length");
		if (sizeString != "")
			contentSize = Converter<size_t>::toValue(sizeString);
		else
			return msg;
		readFile(category, filename, contentSize, socket);	}
	if (filename != "")
	{
		msg.removeAttribute("content-length");
		std::string bodyString =
			"<msg>Message" + category + " " + filename +
			" from Server #" + "</msg>";
		std::string sizeString = Converter<size_t>::toString(bodyString.size());
		msg.addAttribute(HttpMessage::Attribute("content-length", sizeString));
		msg.addBody(bodyString);
	}
	else {
		size_t numBytes = 0;
		size_t pos = msg.findAttribute("content-length");
		if (pos < msg.attributes().size())
		{
			numBytes = Converter<size_t>::toValue(msg.attributes()[pos].second);
			Socket::byte* buffer = new Socket::byte[numBytes + 1];
			socket.recv(numBytes, buffer);
			buffer[numBytes] = '\0';
			std::string msgBody(buffer);
			msg.addBody(msgBody);
			delete[] buffer;	}
	}
return msg;
}
//Function to read the files send by client >--------------------
bool ClientHandler::readFile(const std::string& category, const std::string& filename, size_t fileSize, Socket& socket)
{
std::string fqname =GetRepository(category) + GetFile(filename);
  FileSystem::File file(fqname);
  file.open(FileSystem::File::out, FileSystem::File::binary);
  if (!file.isGood())
  {    Show::write("\n\n  can't open file " + fqname);
    return false;}
  const size_t BlockSize = 2048;
  Socket::byte buffer[BlockSize];
  size_t bytesToRead;
  while (true)
  {	if (fileSize > BlockSize)
      bytesToRead = BlockSize;
    else
      bytesToRead = fileSize;
    socket.recv(bytesToRead, buffer);
    FileSystem::Block blk;
    for (size_t i = 0; i < bytesToRead; ++i)
      blk.push_back(buffer[i]);
    file.putBlock(blk);
    if (fileSize < BlockSize)
      break;
    fileSize -= BlockSize;
  }
  file.close();
  return true;
}

//function to receive the message from client and enqueue them in queue
void ClientHandler::operator()(Socket socket)
{
  while (true)
  {
    HttpMessage msg = readMessage(socket);
    if (connectionClosed_ || msg.bodyString() == "quit")
    {
      Show::write("\n\n  clienthandler thread is terminating");
      break;
    }
    msgQ_.enQ(msg);
  }
}

//function to create te different messages on basis of request type
HttpMessage ServerSender::CreateDiffMsg(const std::string msgType, const EndPoint & myEndPoint, HttpMessage::Attribute attrib, const std::string & body, const EndPoint & ep)
{
	HttpMessage h1;
	h1.clear();
	h1.addAttribute(HttpMessage::attribute(msgType, "Message"));
	h1.addAttribute(HttpMessage::Attribute("mode", "oneway"));
	h1.addAttribute(HttpMessage::parseAttribute("toAddr:" + ep));
	h1.addAttribute(HttpMessage::parseAttribute("fromAddr:" + myEndPoint));
	h1.addBody(body);
	if (body.size() > 0)
	{
		attrib = HttpMessage::attribute("content-length", Converter<size_t>::toString(body.size()));
		h1.addAttribute(attrib);
	}
	return h1;
}

//function to get all files from Server on basis of Category requested by Client
std::string ServerSender::GetfilesFromALLCategory(const std::string categories)
{	std::vector<std::string> allCategoryFiles;
	std::string catFiles="";
	allCategoryFiles = FileSystem::Directory::getFiles(GetRepository(categories), "*.*");
	for (size_t i = 0; i < allCategoryFiles.size(); ++i)
	{	catFiles =allCategoryFiles[i]+","+catFiles;
	}
	std::size_t found = catFiles.find_last_of(",");
	catFiles.substr(0, found);
	return catFiles;
}

//function to get file name from filepath
std::string ClientHandler::GetFile(const std::string& str)
{
	std::size_t found = str.find_last_of("/\\");
	return str.substr(found + 1);
}

//function used for getting all files from repository on category basis
std::string ServerSender::GetRepository(const std::string& category)
{
	return FileSystem::Path::fileSpec(FileSystem::Path::getPath(GetExePath()), "..\\..\\ServerRepository\\"+category);
}

//function to analyse dependency of requested files
std::vector<std::string> ServerSender::AnalyzeFileDependencies(const std::string & file, std::unordered_map<std::string, std::set<std::string>> htmlDepTable)
{	std::vector<std::string>depFiles;
	std::set<std::string>::iterator it_in;
	for (auto it : htmlDepTable) {
		if (GetFile(file) == GetFile(it.first)) {
			for (it_in = it.second.begin(); it_in != it.second.end(); ++it_in) {
				depFiles.push_back(GetFile(*it_in + ".htm"));
			}
			}
	}
	depFiles.push_back(GetFile(file)+".htm");
	return depFiles;
}

//function that return the exe path of execution
std::string ServerSender::GetExePath()
{
	char ownPth[MAX_PATH];
	// Will contain exe path
	HMODULE hModule = GetModuleHandle(NULL);
	if (hModule != NULL)
	{	// When passing NULL to GetModuleHandle, it returns handle of exe itself
		GetModuleFileNameA(hModule, ownPth, (sizeof(ownPth)));
	}
	return std::string(ownPth);
}

//Used for generating files in ServerRepository on category basis
std::string ClientHandler::GetRepository(const std::string& category)
{
	return FileSystem::Path::fileSpec(FileSystem::Path::getPath(GetExePath()), "..\\..\\ServerRepository\\" + category+"\\");
}

//function that return the exe path of execution
std::string ClientHandler::GetExePath()
{
	char ownPth[MAX_PATH];
	// Will contain exe path
	HMODULE hModule = GetModuleHandle(NULL);
	if (hModule != NULL)
	{	// When passing NULL to GetModuleHandle, it returns handle of exe itself
		GetModuleFileNameA(hModule, ownPth, (sizeof(ownPth)));
	}
	return std::string(ownPth);
}

//----< Listener at Server End>--------------------------------------------------
int main()
{ ::SetConsoleTitle(L"HttpMessage Server - Runs Forever");
  ServerSender s1;
  Show::attach(&std::cout);
  Show::start();
  Show::title("\n  HttpMessage Server started");
  BlockingQueue<HttpMessage> msgQ;
  try
  {	 SocketSystem ss;
    SocketListener sl(8080, Socket::IP6);
    ClientHandler cp(msgQ);
    sl.start(cp);
	while (true)
	{	HttpMessage msg = msgQ.deQ();
		std::thread t2([&]() { 
			if (msg.attributes()[0].first == "POST")
			{	Show::write("\n\n Req-5,6,7,8 Server received POST message from CLient:" + msg.bodyString());
				s1.sendFilesReply(msg.bodyString());}
			else if (msg.attributes()[0].first == "PUBLISH") {
				std::cout << "\n\n Req-3,4,6,7,8,10(bonus lazy download) Server received PUBLISH message from CLient:" + msg.bodyString();
				s1.sendFileforPublishRequest(msg.findValue("category"), msg.bodyString());}
			else if (msg.attributes()[0].first == "DELETE") {
				std::cout << "\n\n Req-6,7,8 Server received DELETE message from CLient:" + msg.bodyString();
				s1.ProcessDeleteFileRequest(msg.findValue("category"), msg.bodyString());}
			else if (msg.attributes()[0].first == "GETCATFILES") {
				std::cout << "\n\n Req-5,6,7,8 Server received GET CATEGORY FILES message from Client : " + msg.bodyString();
				std::string allFiles = s1.GetfilesFromALLCategory(msg.bodyString());
				s1.sendAllFilesAcknowldement(allFiles);}
			else if (msg.attributes()[0].first == "DOWNLOADJSCSS") {
				std::cout << "\n\n Req3,4 Server received Download JS and CSS files request from Client : " + msg.bodyString(),msg.findValue("css");
				s1.downloadCSSJSS(msg);
			}
			else if (msg.attributes()[0].first == "NOPARENT") {
				std::cout << "\n\n Req-6,7,8 Server received NOPParent files list request from Client for Category: " + msg.bodyString();
				s1.parentListAcknowledgement(msg.bodyString());
			}
		} 
		); t2.join();
	}
  }
  catch (std::exception& exc)
  { Show::write("\n  Exeception caught: ");
  std::string exMsg = "\n  " + std::string(exc.what()) + "\n\n";
    Show::write(exMsg);
  }
}